.ONESHELL:
SHELL := /bin/bash
SRC = $(wildcard notebooks/*.ipynb)

package_version := $(shell grep "^version" pyproject.toml)
package_version_only := $(shell grep "^version" pyproject.toml | sed 's/^.*[^0-9]\([0-9]*\.[0-9]*\.[0-9]*\).*$$/\1/')

install:
	poetry install
	poetry run python -m ipykernel install --name fiscal-qa --user
	poetry run pre-commit install

precommit:
	poetry run nbdev_clean
	# The - prevent stopping on error
	-poetry run pre-commit run --files fiscal_qa/* notebooks/* models_evaluations/* dataset_generation/* vector_store/*
	$(MAKE) lib
	-poetry run pre-commit run --files fiscal_qa/* notebooks/* models_evaluations/* dataset_generation/* vector_store/*
	-echo "XXXXXXXX Last check XXXXXXXXXXX"
	-poetry run pre-commit run --files fiscal_qa/* notebooks/* models_evaluations/* dataset_generation/* vector_store/*
	
lib:
	rm -rf fiscal_qa/*
	@echo "Set all version to $(package_version_only)"
	sed -i "/^version/c\$(package_version)" settings.ini
	# sed -i '0,/{% set version = "*.*.*"/ s//{% set version = "$(package_version_only)"/' .conda/meta.yaml
	poetry run nbdev_export

docs:
	$(MAKE) doc

doc:
	poetry run nbdev_docs
	# curl https://cdn.plot.ly/plotly-2.14.0.min.js -o _docs/memos/plotly.js

test:
	# poetry run nbdev_test_nbs
	# export PYTHONPATH=$PYTHONPATH:`pwd`/dataset_generation
	poetry run pytest dataset_generation/tests/*

bump:
	poetry version patch
	$(MAKE) precommit

conda_release:
	poetry run fastrelease_conda_package

pypi: dist
	poetry run twine upload --repository pypi dist/*

dist: clean
	poetry run python setup.py sdist bdist_wheel

clean:
	rm -rf dist
