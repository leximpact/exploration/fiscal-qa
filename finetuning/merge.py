"""
Si le merge du model a échoué à la fin du train, ce script va vous sauver.
"""

from train import merge

model_short_name = "mistral"
original_model = "/home/onyxia/work/Mistral-7B-v0.1/"
final_checkpoint = f"results/{model_short_name}/final_checkpoint"
final_merged_checkpoint = "/home/onyxia/mistral-finetuned/"

output_dir = "/home/onyxia/mistral-finetuned/"

merge(original_model, final_checkpoint, final_merged_checkpoint)
