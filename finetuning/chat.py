from transformers import AutoTokenizer
import transformers
import torch
from dataset_generation.openfisca_dataset import BASE_MODELS
import os
from time import time

os.environ["TRANSFORMERS_CACHE"] = "/media/2To-nvme/dev/llm/huggingface/cache/"


model_short_name = "tiny"
model_name = BASE_MODELS[model_short_name]

output_dir = f"results/{model_short_name}/final_merged_checkpoint"
model = output_dir
# model = "PY007/TinyLlama-1.1B-Chat-v0.1"
tokenizer = AutoTokenizer.from_pretrained(model)
pipeline = transformers.pipeline(
    "text-generation",
    model=model,
    torch_dtype=torch.float16,
    device_map="auto",
)

article = """En métropole, en Guadeloupe, en Guyane, en Martinique, à La Réunion, à Saint-Barthélemy, à Saint-Martin et à Saint-Pierre-et-Miquelon,
 son montant est porté à 11,66 € l'heure ; 2° A Mayotte, son montant est fixé à 66,35 € l'heure. A compter du 1er août 2022, le montant du minimum garanti prévu à l'article"""


def format_prompt(texte, description):
    B_INST, E_INST = "[INST] ", " [/INST]"
    B_SYS, E_SYS = "<<SYS>>\n", "\n<</SYS>>\n\n"
    prompt = (
        B_INST
        + B_SYS
        + """Vous êtes un juriste travaillant à l'Assemblée Nationale.
Votre travail consiste à peupler et mettre à jour une base des données nécessaires pour réaliser des simulations fiscales et sociales.
Pour cela, vous devez prendre les textes réglementaires dans lesquelles figurent les informations à jour que l'on vous fourni, et d'extraire précisément l'information spécifique dont vous avez besoin.
Vous ne devez extraire que le montant, et uniquement le montant.
Si vous ne trouver pas la réponse dans le texte vous devez le dire et ne pas chercher à en fournir une autre.
        """
        # + row["context"]
        + E_SYS
        + f"Dans le texte suivant, quel est le montant de '{description}' ?\n"
        + texte
        + E_INST
    )
    # row['text'] = text
    return prompt


# prompt = "What are the values in open source projects?"
formatted_prompt = format_prompt(article, "salaire minimum horaire à Mayotte")

print("Starting generation...")
start = time()
sequences = pipeline(
    formatted_prompt,
    do_sample=True,
    top_k=50,
    top_p=0.7,
    temperature=0.2,
    num_return_sequences=1,
    repetition_penalty=1.1,
    max_new_tokens=500,
)
for seq in sequences:
    print(f"Resultat de {model} : {seq['generated_text']}")
print(f"Generation took {time()-start:.2f}s")
