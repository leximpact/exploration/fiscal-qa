from train import train, merge, load_model, preprocess_dataset, create_bnb_config
from dataset_generation.openfisca_dataset import BASE_MODELS
from codecarbon import EmissionsTracker

model_short_name = "tiny"
model_name = BASE_MODELS[model_short_name]

output_dir = f"results/{model_short_name}/final_checkpoint"
final_merged_checkpoint = f"results/{model_short_name}/final_merged_checkpoint"
bnb_config = create_bnb_config()


# Load model from HF with user's token and with bitsandbytes config
model, tokenizer = load_model(model_name, bnb_config)
train_dataset = preprocess_dataset(model, tokenizer)
eval_dataset = preprocess_dataset(model, tokenizer, "test")

tracker = EmissionsTracker()
tracker.start()
try:
    train(model, tokenizer, train_dataset, eval_dataset, output_dir)

    merge(model_name, output_dir, final_merged_checkpoint)
finally:
    tracker.stop()
