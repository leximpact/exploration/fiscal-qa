# Finetuning de modèles Open source

## Résultats des évaluations

| Nom du modèle           | Plateforme d'entrainement | CO2eq (g) pour le finetuning | Taux de réussite | Temps pour 113 prédictions | Mémoire nécessaire pour l'inférence |
|-------------------------|---------------------------|------------------------------|------------------|----------------------------|----------------------------------|
| TinyLlama-1.1B-chat     | N/A               |             N/A              | 0.8%             | 5 minutes (GTX 1080 Ti)    | 5 Go                             |
| TinyLlama-1.1B (finetuned)| GTX 1080 Ti             | 6g (0.2 kWh)                 | 11.5%            | 5 minutes (GTX 1080 Ti)    | 3 Go                             |
| Mistral-7B-v0.1 (finetuned)| Tesla T4 (Onyxia)      | 40g                          | 23.9%            | N/A                        | N/A                              |
| Mistral-small (Mixtral 8x7B) | N/A | N/A | 73.5% | 4 minutes |  N/A                        | N/A
| Mistral-tiny (7B-instruct ?) | N/A | N/A | 75.2% | 3,5 minutes |  N/A                        | N/A
| Mistral-medium (Prototype) | N/A | N/A | 78,6% | 7 minutes | N/A                        | N/A
| Mistral-large-2402 | N/A | N/A | 83.7% | 4.5 minutes | N/A |
| Llama-2-7b-chat-hf      | N/A           | N/A                          | 12.3%            | 3 minutes (2x A100)        | 20 Go                            |
| Albert (DINUM, Llama-2-13b) | N/A | N/A | 43,4% | 6 minutes | N/A |
| Llama-2-70b-chat-hf       | N/A           | N/A                          | 29.2%            | 12 minutes (2x A100)       | 154 Go                           |
| Llama-2-70b (finetuned)   | A100 (Jean Zay)           | 54g (1.8 kWh)                | 33.6%            | 6 minutes (2x A100)        | 140 Go                           |
| Meta-Llama-3-7B-Instruct | N/A                    | 0.05g (infer. only)                          | 4.4%            | 3 minutes                  | N/A              |
| Meta-Llama-3-70B-Instruct | N/A                    | 0.17g (infer. only)  0.074469 kWh  | 32%      | 8 minutes                  | N/A              |
| GPT-3.5                 | N/A                    | N/A                          | 72.5%            | 7 minutes                  | N/A                              |
| GPT-4                   | N/A                    | N/A                          | 92.9%            | 20 minutes                 | N/A                              |

Consommation et émissions de CO2eq calculées avec https://codecarbon.io/

Le finetuning permet d'obtenir de bien meilleurs résultats que le modèle de base pour TinyLlama-1.1B, mais amélioire à peine les performances pour un gros modèle comme Llama-2-70B.

Les modèles d'OpenAI sont bien meilleurs, mais ils sont propriétaires. Il faut cependant envisager de les utiliser en attendant que les modèles open source soient meilleurs.

On peut améliorer les performances de l'open source en travaillant sur le prompt et sur le dataset. Mais arrivera-t-on à atteindre les performances d'OpenAI de cette façon ?


## Finetuning

```sh
git clone https://git.leximpact.dev/leximpact/fiscal-qa.git
cd fiscal-qa
python -m venv .venv
source .venv/bin/activate
pip install -r finetuning/requirements.txt
```

Le fichier `openfisca_dataset.py` construit un dataset selon le modèle suivant :
```python
chat = [
        {
            "role": "system",
            "content": """Vous êtes un juriste travaillant à l'Assemblée Nationale.
    Votre travail consiste à peupler et mettre à jour une base des données nécessaires pour réaliser des simulations fiscales et sociales.
    Pour cela, vous devez prendre les textes réglementaires dans lesquelles figurent les informations à jour que l'on vous fourni, et d'extraire précisément l'information spécifique dont vous avez besoin.
    Vous ne devez extraire que le montant, et uniquement le montant.
    Si vous ne trouver pas la réponse dans le texte vous devez le dire et ne pas chercher à en fournir une autre.
    Avant de répondre, vérifier que la réponse se trouve bien dans le texte indiqué.
            """,
        },
        {
            "role": "user",
            "content": f"Dans le texte suivant, quel est le montant de '{row['description']}' ?\n"
            + f'"""\n{row["texte"]}\n"""\n',
        },
        {
            "role": "assistant",
            "content": "\n[VALUE]\n" + f'{row["value"]}' + "\n[/VALUE]",
        },
    ]
```

Qui va être transformé par la librairie tokenizer en fonction du modèle utilisé en un seul bloc de texte :

```python
<s>
B_INST
+ B_SYS
+ """Vous êtes un juriste travaillant à l'Assemblée Nationale.
Votre travail consiste à peupler et mettre à jour une base des données nécessaires pour réaliser des simulations fiscales et sociales.
Pour cela, vous devez prendre les textes réglementaires dans lesquelles figurent les informations à jour que l'on vous fourni, et d'extraire précisément l'information spécifique dont vous avez besoin.
Vous ne devez extraire que le montant, et uniquement le montant.
Si vous ne trouver pas la réponse dans le texte vous devez le dire et ne pas chercher à en fournir une autre.
"""
+ E_SYS
+ f"Dans le texte suivant, quel est le montant de '{description du paramètre OpenFisca}' ?\n"
+ {texte_legifrance}
+ E_INST
+ "\n[VALUE]\n"
+ f'{Valeur du paramètre dans OpenFisca}'
+ "\n[/VALUE]"
+ "</s>"
```

Puis le script `finetune.py` va finetuner un modèle sur ce dataset. Il fait appel à train.py qui utilise :
- Transformers avec BitsAndBytes pour réaliser la quantization en 4 bits
- et PEFT avec LoRA
Ainsi l'espace mémoire nécessaire est réduit et PY007/TinyLlama-1.1B-Chat-v0.1 peut être entrainé sur une vieille GTX 1080 ti.

Training de TinyLlama :
![Alt text](images/image.png)

Training de Llama70B :
![Alt text](images/llama70b.png)

## Evaluation

Ouvrir le notebook `models_evaluations/eval_model.ipynb`, indiquer le chemin du modèle à tester et exécuter toutes les cellules.

### Télécharger les modèles HuggingFace

```sh
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
git lfs install
git clone git@hf.co:mistralai/Mistral-7B-v0.1
```

## Utilisation de Jean Zay

Voir http://www.idris.fr/media/ia/guide_nouvel_utilisateur_ia.pdf et http://www.idris.fr/jean-zay/pre-post/jean-zay-jupyterhub.html

A retenir :
- L'accès est limité aux postes internes du réseau de l'Assemblée.
- Accès SSH : `ssh login@jean-zay.idris.f`
- L'accès SSH amène sur une machine qui a de faible capacité de calcul mais accès au réseau.
- JupyterLab est disponible :  https://jupyterhub.idris.fr mais limité à 8h d'utilisation.
- JupyterLab permet de lancer VSCode Server
- Pour aller au delà de 8h de calcul, il faut utiliser SLURM pour demander l'exécution d'un script sur les noeuds de calcul. http://www.idris.fr/jean-zay/gpu/scripts-python-execution-travaux-gpu.html
- Il existe un espace `$WORK` de 5 To par projet sauvegardé. Mais disque "lent" : 100 Go/s en lecture/écriture. => A privilégier pour notre usage.
- Il existe un espace `$SCRATCH` de 2,5 Po partagée non sauvegardé et effacé après 30 jours. Avantage : ce sont des SSD très rapide : 500 Go/s en lecture/écriture
- Il existe un espace `$STORE` de 50 To par projet pour le cold storage : inutile pour nous.
- Il y a déjà un dépot HuggingFace dans `$DSDIR/HuggingFace_Models/`, par exemple `$DSDIR/HuggingFace_Models/meta-llama/Meta-Llama-3-70B-Instruct/`


A faire :
- Configurer le stockage dans WORK :
```sh
mkdir $WORK/.local
ln -s $WORK/.local $HOME
mkdir $WORK/.cache
ln -s $WORK/.cache $HOME
```

## Visualiser les jobs

```sh
squeue -u $USER
```

## Créer l'environnement Python

```sh
cd $WORK/fiscal-qa
python -m venv .venv
source .venv/bin/activate
ipython kernel install --name "venv-huggingface" --user
```

Les noeuds de calcul n'ont pas accès à Internet, mais comme ils ont accès à $WORK, ils partagent l'environnement avec le noeud de connexion qui lui a internet.

On peut donc développer de façon interractive sur un noeud de calcul, en installant les paquets sur le noeud de connexion.

La façon la plus pratique me semble de lancer un Jupyter sur un noeud de calcul, ce qui prend moins de 2 minutes pour accéder à un GPU. Puis de ce Jupyter on peut lancer VS Code Server pour développer et accéder à un terminal.

## Activer l'environnement Python

```sh
cd $WORK/fiscal-qa
source .venv/bin/activate
ipython kernel install --name "venv-fiscal-qa" --user
```

# Utilisation de Llama-70B

Il est disponible dans `/gpfsdswork/dataset/HuggingFace_Models/meta-llama/Llama-2-70b-hf/`
Llama70B necessite 120 go de RAM pour utiliser la version finetunée.
