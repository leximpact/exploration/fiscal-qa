
## Mistral

mc cp -r s3/projet-transformers/models/mistralai/Mistral-7B-v0.1 ~/work/

## Finetuning

Attention, il faut plus de 60Go pour stocker le modèle, l'environnement et le modèle résultant.  

### Résultat

***** train metrics *****
  epoch                    =       6.91
  train_loss               =      0.153
  train_runtime            = 4:18:17.70
  train_samples_per_second =      0.101
  train_steps_per_second   =      0.025
{'train_runtime': 15497.706, 'train_samples_per_second': 0.101, 'train_steps_per_second': 0.025, 'train_loss': 0.15300757079989977, 'epoch': 6.91}


