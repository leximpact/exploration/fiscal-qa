# Liste des PR ouvertes sur openfisca-france dans le cadre de ce projet

1. [Mise à jour last_value_still_valid_on impôt sur le revenu pour les références périmées](https://github.com/openfisca/openfisca-france/pull/2294)
1. [Revalorisations APL étudiants](https://github.com/openfisca/openfisca-france/pull/2287)
1. [Rattrapage de l'historique de prestations_sociales.aides_logement.logement_social.plu.par_categorie_de_menage](https://github.com/openfisca/openfisca-france/pull/2308)
1. [Revalorisation des aides aux logement en métropole et outre-mer](https://github.com/openfisca/openfisca-france/pull/2297)
1. [Mise à jour des last_value_still_valid_on pour la taxation indirecte](https://github.com/openfisca/openfisca-france/pull/2293)
1. [Mise à jour des last_value_still_valid_on pour la taxation capital](https://github.com/openfisca/openfisca-france/pull/2291)
1. [Mise à jour des _last_value_still_valid_on_ pour les prestations sociales](https://github.com/openfisca/openfisca-france/pull/2290)
1. [Mise à jour des _last_value_still_valid_on_ pour les prélèvements sociaux](https://github.com/openfisca/openfisca-france/pull/2289)
1. [Mise à jour des last_value_still_valid_on pour l'impôt sur le revenu](https://github.com/openfisca/openfisca-france/pull/2288)
1. [Mise à jour des urls des références Unedic](https://github.com/openfisca/openfisca-france/pull/2276)
1. [Expand and update accise_energie_metropole](https://github.com/openfisca/openfisca-france-indirect-taxation/pull/183)
1. [Mise à jour des paramètres d'imposition des rentes viagères](https://github.com/openfisca/openfisca-france/pull/2332) - A trouvé la référence automatiquement en utilisant un agent.
1. [Indemnité de résidence dans la fonction publique](https://github.com/openfisca/openfisca-france/pull/2335) - N'a pas réussi à trouvé tout seul la référence législative.
