"""
Convert the dataset to a json file
"""
import json
import pandas as pd

df = pd.read_csv("./data/openfisca_parameters_with_texte_and_value_in_texte.csv")

jsonl = []


def to_json(row):
    chat = [
        {
            "role": "system",
            "content": """Vous êtes un juriste travaillant à l'Assemblée Nationale.
    Votre travail consiste à peupler et mettre à jour une base des données nécessaires pour réaliser des simulations fiscales et sociales.
    Pour cela, vous devez prendre les textes réglementaires dans lesquelles figurent les informations à jour que l'on vous fourni, et d'extraire précisément l'information spécifique dont vous avez besoin.
    Vous ne devez extraire que le montant, et uniquement le montant.
    Si vous ne trouver pas la réponse dans le texte vous devez le dire et ne pas chercher à en fournir une autre.
    Avant de répondre, vérifier que la réponse se trouve bien dans le texte indiqué.
            """,
        },
        {
            "role": "user",
            "content": f"Dans le texte suivant, quel est la valeur de '{row['description']}' ?\n"
            + f'"""\n{row["texte"]}\n"""\n',
        },
        {
            "role": "assistant",
            "content": "\n[VALUE]\n" + f'{row["value"]}' + "\n[/VALUE]",
        },
    ]
    jsonl.append(chat)


df.apply(to_json, axis=1)

with open("./data/openfisca_dataset.jsonl", "w") as f:
    for line in jsonl:
        f.write(json.dumps(line[0]) + "\n")
