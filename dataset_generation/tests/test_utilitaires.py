import unittest
from utilitaires import extract_dates, formatNumber, extract_floats, surlignage, find_value_in_texte, get_text_from_html

"""
export PYTHONPATH=$PYTHONPATH:`pwd`/dataset_generation
pytest dataset_generation/tests/*
"""

class TestUtilitaires(unittest.TestCase):

    def test_extract_dates(self):
        self.assertEqual(extract_dates("The date is 2022/09/30"), ["2022/09/30"])
        self.assertEqual(extract_dates("No date here"), [])

    def test_formatNumber(self):
        self.assertEqual(formatNumber(5.0), 5)
        self.assertEqual(formatNumber(5.5), 5.5)

    def test_extract_floats(self):
        self.assertEqual(
            extract_floats(
                "cette valeur est de 182,23 euros. Donc, la nouvelle valeur est : 182.23."
            )[-1],
            182.23,
        )
        self.assertEqual(
            extract_floats(
                "The Plafond Réunion is 2 450 €. The float number extracted from this is 2,450.5."
            )[-1],
            2450.5,
        )
        self.assertEqual(extract_floats("2450.566")[-1], 2450.566)
        self.assertEqual(extract_floats(" 2450.")[-1], 2450)
        self.assertEqual(extract_floats("2250.")[-1], 2250)
        self.assertEqual(extract_floats("La nouvelle valeur est : 0.25.")[-1], 0.25)
        self.assertEqual(extract_floats(" 0.25.")[-1], 0.25)
        self.assertEqual(extract_floats("The price is 5.5"), [5.5])
        self.assertEqual(extract_floats("No float here"), [])

    def test_surlignage(self):
        self.assertEqual(surlignage("Hello world", "world"), "Hello <mark>world</mark>")
        self.assertEqual(surlignage("Hello world", "universe"), "Hello world")

    def test_find_value_in_texte(self):

        self.assertFalse(find_value_in_texte("de 6 250 € ", 25, debug=False))
        self.assertEqual(find_value_in_texte(r" 85%.", 85), "85")

        self.assertEqual(find_value_in_texte("de 6 250 € ", 6250.00, debug=False), "6 250")
        self.assertFalse(find_value_in_texte(" 2 ", 0.018, debug=True))
        self.assertEqual(find_value_in_texte(" réduit de 1,8 point ", 0.018), "1,8")

        self.assertEqual(
            find_value_in_texte(
                "augmenté d'une demi-part pour chaque enfant à charge", 0.5, debug=False
            ),
            "demi",
        )
        self.assertEqual(
            find_value_in_texte(
                "2,74 % pour l'année 295 et de 2,95 % à compter du 1er janvier 2021 ;",
                0.0295, debug=True
            ),
            "2,95",
        )
        self.assertEqual(
            find_value_in_texte("Le taux de la contribution employeur est de 5 %", 0.05),
            "5",
        )
        self.assertEqual(
            find_value_in_texte("à retenue pour pension, est fixé à 9,70 %.", 0.097),
            "9,70",
        )
        self.assertEqual(
            find_value_in_texte(
                r"2003 est égale à 3 % du prix de revient du logement", 0.03, debug=False
            ),
            "3",
        )
        self.assertEqual(find_value_in_texte(r"<br>8,50 % <br>", 0.085, debug=False), "8,50")
        self.assertEqual(find_value_in_texte(r"<br>85<br>", 85), "85")
        self.assertEqual(find_value_in_texte(r"<br>3,7 </td>", 3.7), "3,7")
        self.assertEqual(find_value_in_texte(r"fixé à 0,44 % pour", 0.0044), "0,44")
        self.assertEqual(find_value_in_texte(r"fixé à 4% pour", 0.04), "4")
        self.assertEqual(
            find_value_in_texte(r"à partir de onze ans", 11.0, debug=False), "onze"
        )
        self.assertEqual(find_value_in_texte(r"à 37,20 euros ", 37.2, debug=False), "37,20")
        self.assertEqual(find_value_in_texte(r"à 42,804 % ", 0.42804, debug=False), "42,804")
        self.assertFalse(
            find_value_in_texte(r"représentant 0,45 % au moins", 0, debug=False)
        )
        # False car nous cherchons dans des phrases complètes
        self.assertFalse(find_value_in_texte("The price is 5.5", 5.5, debug=True))
        self.assertFalse(find_value_in_texte("The price is 5.5", 6.5))


    def test_get_text_from_html(self):
        self.assertEqual(get_text_from_html("<p>Hello world</p>"), "Hello world")
        self.assertEqual(get_text_from_html("<div><p>Hello</p><p>world</p></div>"), "Helloworld")
        self.assertEqual(get_text_from_html("<p>test</p>"), "test")

if __name__ == '__main__':
    unittest.main()

