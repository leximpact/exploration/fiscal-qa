import unittest
import tempfile
from unittest.mock import patch
from openfisca_param import update_openfisca_parameter, convert_str_to_date,df_to_markdown, check_same_value, create_url_legifrance
import ruamel.yaml
import datetime
import pandas as pd

yaml = ruamel.yaml.YAML()
class TestOpenFiscaParam(unittest.TestCase):

    @patch('os.path.exists', return_value=False)
    def test_update_openfisca_parameter_file_not_found(self, mock_exists):
        chemin = "non_existent_file.yaml"
        old_date = "2022-09-30"
        new_date = "2022-10-01"
        id_new_ref = "123"
        new_value = 5.5
        date_application = "2022-10-01"
        titre_court = "Test"
        result =         update_openfisca_parameter(
            chemin=chemin, 
            last_value_still_valid_on_new=new_date, 
            last_value_still_valid_on_old=old_date, 
            id_new_ref=id_new_ref, 
            new_value=new_value, 
            date_application=date_application, 
            titre_court=titre_court
        )
        self.assertIsNone(result)
        mock_exists.assert_called_once_with("../../openfisca-france/" + chemin)

    def test_update_openfisca_parameter_new_value_not_float(self):
        chemin = "existent_file.yaml"
        old_date = "2022-09-30"
        new_date = "2022-10-01"
        id_new_ref = "123"
        new_value = "not_a_float"
        date_application = "2022-10-01"
        titre_court = "Test"
        result = update_openfisca_parameter(
            chemin=chemin, 
            last_value_still_valid_on_new=new_date, 
            last_value_still_valid_on_old=old_date, 
            id_new_ref=id_new_ref, 
            new_value=new_value, 
            date_application=date_application, 
            titre_court=titre_court
        )
        self.assertIsNone(result)
    
    def test_update_openfisca_parameter_date_application_empty(self):
        chemin = "existent_file.yaml"
        old_date = "2022-09-30"
        new_date = "2022-10-01"
        id_new_ref = "123"
        new_value = 5.5
        date_application = ""
        titre_court = "Test"
        result = update_openfisca_parameter(
            chemin=chemin, 
            last_value_still_valid_on_new=new_date, 
            last_value_still_valid_on_old=old_date, 
            id_new_ref=id_new_ref, 
            new_value=new_value, 
            date_application=date_application, 
            titre_court=titre_court
        )
        self.assertIsNone(result)

    def test_update_openfisca_parameter_date_only_dont_exist(self):
        # Get temp file pointer
        temp_dir = tempfile.mkdtemp()
        tmp_file = "test_update_openfisca_parameter_date_only_dont_exist.yaml"
        tmp_filepath = temp_dir + "/" + tmp_file
        with open(tmp_filepath, "w") as f:
            f.write("""
description: Personnes
values:
  2017-10-01:
    value: 317.47
metadata:
                    """)
        date_last_value = "2024-09-28"
        result = update_openfisca_parameter(tmp_file, last_value_still_valid_on_old=None, last_value_still_valid_on_new=date_last_value,
                                               root_path=temp_dir)
        self.assertIsNotNone(result)
        print(result)

        with open(tmp_filepath) as f:
            of_param = yaml.load(f)
        self.assertEqual(of_param["metadata"]["last_value_still_valid_on"], date_last_value)

    def test_update_openfisca_parameter_date_only(self):
        # Get temp file pointer
        temp_dir = tempfile.mkdtemp()
        tmp_file = "test_update_openfisca_parameter_date_only.yaml"
        tmp_filepath = temp_dir + "/" + tmp_file
        with open(tmp_filepath, "w") as f:
            f.write("""
description: Personnes
values:
  2017-10-01:
    value: 317.47
metadata:
  last_value_still_valid_on: "2022-09-28"
                    """)
        date_last_value = "2024-09-28"
        result = update_openfisca_parameter(tmp_file, last_value_still_valid_on_old="2022-09-28", last_value_still_valid_on_new=date_last_value,
                                               root_path=temp_dir)
        self.assertIsNotNone(result)
        print(result)

        with open(tmp_filepath) as f:
            of_param = yaml.load(f)
        self.assertEqual(of_param["metadata"]["last_value_still_valid_on"], date_last_value)

    def test_update_openfisca_parameter_add_ref(self):
        # TODO: Not coded yet
        pass

    def test_update_openfisca_parameter_date_and_value_and_ref(self):
        # Get temp file pointer
        temp_dir = tempfile.mkdtemp()
        tmp_file = "test_update_openfisca_parameter_date_and_value_and_ref.yaml"
        tmp_filepath = temp_dir + "/" + tmp_file
        with open(tmp_filepath, "w") as f:
            f.write("""
description: Personnes
values:
  2017-10-01:
    value: 317.47
metadata:
  last_value_still_valid_on: "2022-09-28"
  reference:
    2017-10-01:
      title: Arrêté du 28/09/2017, art. 11
      href: https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000035665903
  official_journal_date:
    2023-10-01: "2023-09-27"
                    """)
        date_application = "2024-01-01"
        date_last_value = "2024-09-28"
        new_value = 1235.45
        result = update_openfisca_parameter(tmp_file, last_value_still_valid_on_old="2022-09-28", last_value_still_valid_on_new=date_last_value, id_new_ref="NEW_REF", new_value=new_value, date_application=date_application, titre_court= "Test",
                                               root_path=temp_dir)
        self.assertIsNotNone(result)
        print(result)

        with open(tmp_filepath) as f:
            of_param = yaml.load(f)
        date_application_in_datetime = convert_str_to_date(date_application)
        self.assertEqual(of_param["metadata"]["reference"][date_application_in_datetime]["title"], "Test")
        self.assertEqual(of_param["metadata"]["last_value_still_valid_on"], date_last_value)
        self.assertEqual(of_param["values"][date_application_in_datetime]["value"], new_value)


    def test_df_to_markdown(self):
        data = {
            'name': ['param1'],
            'value_in_nouveau_ref_found': ['5.5'],
            'id_version_en_vigueur': ['123'],
            'value_in_nouveau_ref_float': ['5.5'],
            'value_in_nouveau_ref_found': ['5,5'],
            'value_in_nouveau_ref_texte': ['<p>5,5</p>'],
            'date_application'  : ['2022-10-01'],
            'description': ['Test parameter'],
            'github_url': ['https://github.com/test/param1'],
            'value': ['4.4']
        }
        df = pd.DataFrame(data)
        markdown = df_to_markdown(df, mode="new_value", include_list=['param1'])
        expected_md = "| Paramètre | Ancienne valeur | Extrait |  Nouvelle valeur trouvée | Valeur trouvée dans | \r\n| --- | --- | --- | --- | --- |  \r\n| Test parameter [param1](https://github.com/test/param1) | 4.4 | 5,5 | 5.5 | [123](https://www.legifrance.gouv.fr/codes/article_lc/123/2022-10-01/#:~:text=5%2C5) |\r\n"
        self.assertEqual(markdown, expected_md)

    def test_check_same_value_equal_values(self):
        value = 5.5
        value_in_ref = "5,5"
        result = check_same_value(value, value_in_ref)
        self.assertTrue(result)
        self.assertTrue(check_same_value(0.5, "demi"))
    def test_check_same_value_equal_pct(self):
        value = 0.055
        value_in_ref = "5.5"
        result = check_same_value(value, value_in_ref)
        self.assertTrue(result)

    def test_check_same_value_close_values(self):
        value = 0.1
        value_in_ref = "0.1001"
        result = check_same_value(value, value_in_ref)
        self.assertFalse(result)

    def test_check_same_value_not_equal_values(self):
        value = 3.14
        value_in_ref = "2.718"
        result = check_same_value(value, value_in_ref)
        self.assertFalse(result)

    def test_check_same_value_invalid_value_in_ref(self):
        value = 10.0
        value_in_ref = "not_a_float"
        result = check_same_value(value, value_in_ref)
        self.assertFalse(result)

    def test_create_url_legifrance_with_id_legifrance_and_date_and_value_in_ref(self):
        url = create_url_legifrance(id_legifrance="123", date="2022-10-01", value_in_ref="5.5")
        expected_url = "https://www.legifrance.gouv.fr/codes/article_lc/123/2022-10-01/#:~:text=5.5"
        self.assertEqual(url, expected_url)

    def test_create_url_legifrance_with_id_legifrance_and_date(self):
        url = create_url_legifrance(id_legifrance="123", date="2022-10-01")
        expected_url = "https://www.legifrance.gouv.fr/codes/article_lc/123/2022-10-01/"
        self.assertEqual(url, expected_url)

    def test_create_url_legifrance_with_url_legi_and_date_and_value_in_ref(self):
        url = create_url_legifrance(url_legi="https://www.example.com", date="2022-10-01", value_in_ref="5.5")
        expected_url = "https://www.example.com/2022-10-01/#:~:text=5.5"
        self.assertEqual(url, expected_url)

    def test_create_url_legifrance_with_url_legi_and_date(self):
        url = create_url_legifrance(url_legi="https://www.example.com", date="2022-10-01")
        expected_url = "https://www.example.com/2022-10-01/"
        self.assertEqual(url, expected_url)

    def test_create_url_legifrance_with_url_legi_and_value_in_ref(self):
        url = create_url_legifrance(url_legi="https://www.example.com", value_in_ref="5.5")
        expected_url = "https://www.example.com/#:~:text=5.5"
        self.assertEqual(url, expected_url)

if __name__ == '__main__':
    unittest.main()