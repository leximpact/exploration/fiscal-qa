
"""
export PYTHONPATH=$PYTHONPATH:`pwd`/dataset_generation
pytest dataset_generation/tests/*
"""

import unittest
from utilitaires import find_value_in_texte, get_text_from_html
from legidb import LegiDB, find_version_id_from_date, extract_id_from_url

legi = LegiDB()

class TestLegiDB(unittest.TestCase):
    
    def test_extract_id_from_url(self):
        self.assertEqual(
            extract_id_from_url(
                "https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000038496156"
            ),
            "JORFARTI000038496156",
        )

    def test_find_version_id_from_date(self):
        ##############################################
        # Test find_version_id_from_date
        raw_article_json = legi.get_raw_article_json("LEGIARTI000038869220")
        self.assertEqual(
            find_version_id_from_date(raw_article_json, "2021-01-01"),
            "LEGIARTI000038869220",

        )

    def test_get_titre(self):
        raw_article_json = legi.get_raw_article_json("LEGIARTI000038869220")
        self.assertEqual(
            legi.get_titre(raw_article_json)["long"],
            "Décret n° 2019-797 du 26 juillet 2019 relatif au régime d'assurance chômage - Section 3 : Allocation journalière - Chapitre 4 : Détermination de l'allocation journalière - Titre I : L'ALLOCATION D'AIDE AU RETOUR À L'EMPLOI - RÈGLEMENT D'ASSURANCE CHÔMAGE - Annexe A - Annexes",
        )
        self.assertEqual(
            legi.get_titre(raw_article_json)["court"],
            "Art. 14 du Décret n° 2019-797 du 26 juillet 2019 relatif au régime d'assurance chômage",
        )
        raw_article_json = legi.get_raw_article_json("LEGISCTA000043701827")
        self.assertEqual(
            legi.get_titre(raw_article_json)["long"],
            "Code du travail - Titre IV : Financement de l'apprentissage - Livre II : L'apprentissage - Sixième partie : La formation professionnelle tout au long de la vie - Partie législative",
        )
        self.assertEqual(
            legi.get_titre(raw_article_json)["court"],
            "Code du travail - Chapitre II : Contribution supplémentaire à l'apprentissage",
        )
        raw_article_json = legi.get_raw_article_json("LEGIARTI000048723703")
        self.assertEqual(
            legi.get_titre(raw_article_json)["court"],
            "Art. D5122-13 du Code du travail",
        )
        raw_article_json = legi.get_raw_article_json("JORFTEXT000026863441")
        self.assertEqual(
            legi.get_titre(raw_article_json)["court"], "JORF n°0304 du 30 décembre 2012"
        )
    def test_get_id_version_en_vigueur(self):
        ##############################################
        # Test get_id_version_en_vigueur
        raw_article_json = legi.get_raw_article_json("LEGIARTI000038869220")
        self.assertEqual(
            legi.get_id_version_en_vigueur(raw_article_json), "LEGIARTI000038869220"
        )
    def test_get_versions(self):
        ##############################################
        # Test get_versions
        raw_article_json = legi.get_raw_article_json("LEGIARTI000038869220")
        self.assertEqual(len(legi.get_versions(raw_article_json)), 1)
        raw_article_json = legi.get_raw_article_json("LEGIARTI000049641925")
        self.assertEqual(len(legi.get_versions(raw_article_json)), 63)
    def tet_get_article(self):
        ##############################################
        # Test get_article
        article = legi.get_article("LEGIARTI000038869220", debug=False)
        self.assertEqual(article["etat"], "VIGUEUR")
    def test_get_article_from_url_and_date(self):
        ##############################################
        # Test get_article_from_url_and_date
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000038496156",
            "2021-01-01",
            debug=False,
        )
        self.assertEqual(article["etat"], "VIGUEUR")
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033817781/2001-01-01",
            "2001-01-01",
            debug=False,
        )
        self.assertEqual(article["etat"], "MODIFIE")
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000000000000/2001-01-01",
            "2021-01-01",
            debug=False,
        )
        self.assertEqual(article, False)
    def test_get_sql_from_id(self):
        ##############################################
        # Test get_sql_from_id
        self.assertEqual(
            str(legi.get_sql_from_id("LEGIARTI000038869220")),
            "SELECT id, data FROM article WHERE id='LEGIARTI000038869220';",
        )
        self.assertEqual(
            str(legi.get_sql_from_id("LEGIARTI000042837182")),
            "SELECT id, data FROM article WHERE id='LEGIARTI000042837182';",
        )
        self.assertEqual(
            str(legi.get_sql_from_id("JORFTEXT000038496156")),
            "SELECT id, data FROM textelr WHERE id='JORFTEXT000038496156';",
        )
        self.assertEqual(
            str(legi.get_sql_from_id("LEGISCTA000038496156")),
            "SELECT id, data FROM section_ta WHERE id='LEGISCTA000038496156';",
        )
        with self.assertRaises(ValueError):
            legi.get_sql_from_id("toto")
        ##############################################
        # Test is_article_en_vigueur
    def test_is_article_en_vigueur(self):
        with self.assertRaises(ValueError):
            legi.is_article_en_vigueur("toto")
        self.assertFalse(legi.is_article_en_vigueur("LEGIARTI000042837182"))
        with self.assertRaises(KeyError):
            legi.is_article_en_vigueur("JORFARTI000038496156")
        self.assertTrue(legi.is_article_en_vigueur("LEGIARTI000038869220"))
    def test_get_all_textes(self):
        ##############################################
        # Test get_all_textes
        raw_article_json = legi.get_raw_article_json("LEGISCTA000043701827", debug=False)
        self.assertTrue(
            legi.get_all_textes(raw_article_json)["html"].startswith(
                "<br/>   Peuvent être habilités à collect"
            )
        )
        self.assertEqual(legi.get_all_textes(raw_article_json)["nature"], "CODE")
        raw_article_json = legi.get_raw_article_json("JORFTEXT000038496102", debug=False)
        self.assertEqual(
            legi.get_all_textes(raw_article_json)["html"][:20], "<p>\n<br/>I.-Le code "
        )
        self.assertEqual(legi.get_all_textes(raw_article_json)["nature"], "LOI")
        raw_article_json = legi.get_raw_article_json("JORFTEXT000000688994", debug=False)
        self.assertEqual(legi.get_all_textes(raw_article_json)["html"], "")
        self.assertEqual(legi.get_all_textes(raw_article_json)["nature"], "")
    def test_get_article(self):
        ##############################################
        # Test get_article
        article = legi.get_article("LEGIARTI000038869220", debug=False)
        self.assertEqual(article["etat"], "VIGUEUR")
        article = legi.get_article("LEGISCTA000043701827", debug=False)
        self.assertEqual(article["etat"], "MULTIPLE")
        self.assertTrue(
            article["content_html"].startswith("<br/>   Peuvent être habilités à collect")
        )

        # Decret modificatif
        article = legi.get_article("LEGIARTI000049641925", debug=False)
        self.assertEqual(article["decret_modificatif"]["id"], "JORFTEXT000049629761")
        self.assertEqual(article["decret_modificatif"]["date"], "2024-05-30")
        self.assertEqual(article["decret_modificatif"]["nom"], 'Décret n°2024-496 du 30 mai 2024 - art. 1')

        # https://legal.tricoteuses.fr/textes/JORFTEXT000000701672
        # https://legal.tricoteuses.fr/textelr/JORFTEXT000000701672
        article = legi.get_article_from_url_and_date(
            url="https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000701672",
            date="2021-01-01",
            debug=False,
        )
        # article = legi.get_article("JORFTEXT000000701672", debug=True)
        self.assertEqual(article["etat"], "MULTIPLE")
        self.assertEqual(
            article["content_html"][:41], "<br/>   A titre exceptionnel et provisoir"
        )
        article = legi.get_article("LEGIARTI000044981935", debug=False)
        self.assertTrue(" 0,44 % " in article["content_html"])
        self.assertEqual(find_value_in_texte(article["content_html"], 0.0044, debug=True), "0,44")

        article = legi.get_article_from_url_and_date(
            url="https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046816473/2024-01-01/",
            date="2025-01-01",
            debug=False,
        )
        self.assertEqual(
            find_value_in_texte(article["content_html"], 192.3, debug=False), "192,3"
        )

        # Type LEGISTA
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/codes/id/LEGISCTA000043701827",
            "2015-01-01",
            debug=False,
        )
        self.assertEqual(article["etat"], "MULTIPLE")
        self.assertGreater(len(article["content_html"]), 5000)
        # Type JORFSCTA
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/jorf/id/JORFSCTA000036580381",
            "2021-01-01",
            debug=False,
        )
        self.assertEqual(
            article["content_html"][:40], "<p>Article 6<br/>\nEmplois en faveur des "
        )

        # Gestion des dates de version sur LEGIARTI
        article = legi.get_article_from_url_and_date(
            "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000036679615/",
            "2018-01-01",
            debug=False,
        )
        self.assertEqual(find_value_in_texte(article["content_html"], 0.13), "13")
