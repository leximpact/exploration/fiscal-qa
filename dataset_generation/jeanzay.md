Jean Zay

wget https://git.leximpact.dev/leximpact/exploration/fiscal-qa/-/archive/openfisca_param/fiscal-qa-openfisca_param.tar.gz
tar -xvzf fiscal-qa-openfisca_param.tar.gz

curl -sSL https://install.python-poetry.org | python3 -
export PATH="/linkhome/rech/gendxh01/uei48xr/.local/bin:$PATH"
poetry config --local virtualenvs.in-project true

[uei48xr@jean-zay-srv2: fiscal-qa-openfisca_param]$ poetry install
Creating virtualenv fiscal-qa-XoF4R3YP-py3.11 in /linkhome/rech/gendxh01/uei48xr/.cache/pypoetry/virtualenvs

cp -ra /linkhome/rech/gendxh01/uei48xr/.cache/pypoetry/virtualenvs/fiscal-qa-XoF4R3YP-py3.11 /gpfsscratch/rech/gendxh01/uei48xr/fiscal-qa-openfisca_param/.venv