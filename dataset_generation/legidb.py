from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy import text
from decouple import Config, RepositoryEnv
from utilitaires import get_text_from_html

import re
import requests
import dateutil.relativedelta
from glom import glom


def get_redirected_url(url, debug=False):
    """
    Retourne l'URL finale après redirection.
    Exemple https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000036358456&cidTexte=JORFTEXT000036339090
      devient https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000042683772
    """
    response = requests.get(url)
    if response.history:
        url = response.url
    return url


def extract_id_from_url(urls):
    """
    Extrait l'identifiant d'une variable OpenFisca à partir de son URL.
    JORFARTI000038496156
    LEGISCTA000043701827
    JORFTEXT000043701828
    """

    if isinstance(urls, str):
        urls = [urls]
    for url in urls:
        match = re.search(r"LEGI\w{4}\d+", url)
        if match:
            return match.group(0)
        match = re.search(r"JORF\w{4}\d+", url)
        if match:
            return match.group(0)
    return None


def is_list_of_articles(id: str) -> bool:
    return (
        id.startswith("JORFTEXT")
        or id.startswith("LEGISCTA")
        or id.startswith("JORFSCTA")
        or id.startswith("LEGITEXT")
    )


def find_version_id_from_date(raw_article_json, date, debug=False):
    """
    Return the version id of the article at the given date.
    """
    if isinstance(date, str):
        try:
            date = datetime.strptime(date, "%Y-%m-%d")
        except ValueError:
            print(f"ERROR : Date format {date} is not correct")
            return None
    if raw_article_json.get("VERSIONS") is None:
        return None
    for version in raw_article_json["VERSIONS"]["VERSION"]:
        if version.get("LIEN_ART"):
            link_key = "LIEN_ART"
        elif version.get("LIEN_TXT"):
            link_key = "LIEN_TXT"
        try:
            # On retire un mois car souvent les lois sont un peu rétroactives
            debut = datetime.strptime(version[link_key]["@debut"], "%Y-%m-%d")
            debut = debut - dateutil.relativedelta.relativedelta(months=4)
            fin = datetime.strptime(version[link_key]["@fin"], "%Y-%m-%d")
            if debug:
                print(
                    f"find_version_id_from_date - Check version {debut} to {fin} for {date}"
                )
        except ValueError:
            print(
                f"find_version_id_from_date - ERROR : Date format {version[link_key]['@debut']} or {version[link_key]['@fin']} is not correct"
            )
            continue
        if debut <= date < fin:
            # print(f"Found version {version[link_key]['@id']} at date {date}")
            return version[link_key]["@id"]
    return None


class LegiDB:
    def __init__(self):
        try:
            env_config = Config(RepositoryEnv("../.env"))
        except FileNotFoundError:
            env_config = Config(RepositoryEnv(".env"))

        db_host = env_config("LEGI_DB_HOST")
        db_port = env_config("LEGI_DB_PORT")
        db_user = env_config("LEGI_DB_USER")
        db_pass = env_config("LEGI_DB_PASS")
        db_name = env_config("LEGI_DB_NAME")
        db_url = f"postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"
        self.engine = create_engine(
            db_url, client_encoding="utf-8", connect_args={"connect_timeout": 5}
        )
        # Check db is connected
        with self.engine.connect() as con:
            sql = text("SELECT current_database();")
            result = con.execute(sql)
            rows = result.all()
            print("INFO : Connected to database ", rows[0][0])

    def is_article_en_vigueur(self, id: str) -> bool:
        with self.engine.connect() as con:
            sql = self.get_sql_from_id(id)
            result = con.execute(sql)
            rows = result.all()
            if len(rows) == 1:
                article_content = rows[0][1]
                try:
                    if (
                        article_content["META"]["META_SPEC"]["META_ARTICLE"]["ETAT"]
                        == "VIGUEUR"
                    ):
                        return True
                    else:
                        return False
                except KeyError:
                    raise KeyError(
                        f'Article {id} do not have keys ["META"]["META_SPEC"]["META_ARTICLE"]["ETAT"]'
                    )
            else:
                raise ValueError(f"Article {id} not found in database : {sql}")

    @staticmethod
    def get_nature_document(article_data, id: str):
        if id.startswith("JORFTEXT"):
            return "JORFTEXT"
        try:
            return glom(article_data, "CONTEXTE.TEXTE.@nature")
        except Exception:
            # print(f"ERROR : Nature document not found in article data {e}")
            return ""

    @staticmethod
    def get_titre(article_data):
        def recurse_titre(data, key="#text"):
            texts = []
            if isinstance(data, dict):
                for k, v in data.items():
                    if k == key:
                        texts.append(v)
                    else:
                        texts.extend(recurse_titre(v))
            elif isinstance(data, list):
                for item in data:
                    texts.extend(recurse_titre(item))
            return texts

        if article_data.get("CONTEXTE") is None:
            try:
                return {
                    "long": article_data["META"]["META_SPEC"]["META_TEXTE_CHRONICLE"][
                        "ORIGINE_PUBLI"
                    ],
                    "court": article_data["META"]["META_SPEC"]["META_TEXTE_CHRONICLE"][
                        "ORIGINE_PUBLI"
                    ],
                }
            except KeyError:
                return {
                    "long": "",
                    "court": "",
                }
        if article_data["CONTEXTE"]["TEXTE"].get("TM") is None:
            titre = article_data["CONTEXTE"]["TEXTE"]["TITRE_TXT"][-1]["#text"]
            court = titre
        else:
            titres = [article_data["CONTEXTE"]["TEXTE"]["TITRE_TXT"][-1]["#text"]]
            titres += recurse_titre(article_data["CONTEXTE"]["TEXTE"].get("TM"))
            titre = " - ".join(titres)
            court = article_data["CONTEXTE"]["TEXTE"]["TITRE_TXT"][-1]["#text"]
            if article_data.get("META"):
                try:
                    court = (
                        f'Art. {article_data["META"]["META_SPEC"]["META_ARTICLE"]["NUM"]} du '
                        + court
                    )
                except KeyError:
                    pass
            elif article_data.get("TITRE_TA"):
                court += " - " + article_data["TITRE_TA"]
        return {
            "long": titre,
            "court": court,
        }

    @staticmethod
    def get_sql_from_id(id: str, debug: bool = False):
        if id.startswith("LEGISCTA"):
            # https://legal.tricoteuses.fr/section_ta/LEGISCTA000043701827
            sql = text(f"SELECT id, data FROM section_ta WHERE id='{id}';")
        elif id.startswith("JORFTEXT") or id.startswith("LEGITEXT"):
            # https://legal.tricoteuses.fr/textelr/LEGITEXT000006063966
            sql = text(f"SELECT id, data FROM textelr WHERE id='{id}';")
        elif id.startswith("LEGIART") or id.startswith("JORFARTI"):
            sql = text(f"SELECT id, data FROM article WHERE id='{id}';")
        elif id.startswith("JORFSCTA"):
            # https://legal.tricoteuses.fr/section_ta/JORFSCTA000036580381
            sql = text(f"SELECT id, data FROM section_ta WHERE id='{id}';")
        else:
            raise ValueError(f"ID type not recognized : {id}")
        if debug:
            print("get_raw_article_json", sql)
        return sql

    @staticmethod
    def get_id_version_en_vigueur(article_data):
        """
        Retourne l'identifiant de la dernière version en vigueur de l'article.
        'ETAT' peut ne pas exister pour certains articles, comme https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000038496156.
        """
        if article_data.get("META") is None:
            return None
        if article_data["META"]["META_SPEC"].get("META_ARTICLE") is None:
            return None
        if article_data["META"]["META_SPEC"]["META_ARTICLE"].get("ETAT") == "VIGUEUR":
            return article_data["META"]["META_COMMUN"]["ID"]
        else:
            for version in article_data["VERSIONS"]["VERSION"]:
                if version.get("@etat") == "VIGUEUR":
                    return version["LIEN_ART"]["@id"]
        return None

    @staticmethod
    def get_versions(article_data: dict) -> dict:
        """
        Retourne la liste des versions de l'article.
        'ETAT' peut ne pas exister pour certains articles, comme https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000038496156.
        """
        assert isinstance(article_data, dict)
        versions = {}
        if article_data.get("META") is None:
            return versions
        if article_data["META"]["META_SPEC"].get("META_ARTICLE") is None:
            return versions

        """
            @id:  "LEGIARTI000006829031"
            @fin:  "1992-09-03"
            @num:  "Annexe I"
            @etat:  "MODIFIE"
            @debut:  "1987-08-19"
            @origine:  "LEGI"
        """
        for version in article_data["VERSIONS"]["VERSION"]:
            version = version["LIEN_ART"]
            if version["@debut"] == "2999-01-01":
                continue
            versions[version["@debut"]] = {
                "id": version["@id"],
                "debut": version["@debut"],
                "fin": version["@fin"],
                "num": version["@num"],
                "etat": version.get("@etat"),
                "origine": version["@origine"],
            }
        return versions

    def get_all_textes(
        self, raw_json: dict, list_only: bool = False, debug: bool = False
    ):
        """
        Récupère les textes de la base de données pour une liste donnée.
        Dédié aux articles de type JORFTEXT, JORFSCTA et LEGISCTA.
        """
        assert isinstance(raw_json, dict)
        list_of_legiarti = []
        keys = ["STRUCTURE_TA", "STRUCT"]

        for key in keys:
            if raw_json.get(key) and raw_json.get(key).get("LIEN_ART"):
                list_of_legiarti = [arti["@id"] for arti in raw_json[key]["LIEN_ART"]]
                break
            if raw_json.get(key) and raw_json.get(key).get("LIEN_SECTION_TA"):
                list_of_legiarti = [
                    arti["@id"] for arti in raw_json[key]["LIEN_SECTION_TA"]
                ]
                break
        else:
            if debug:
                print("get_all_textes - No link found in database")
            return {
                "html": "",
                "nature": "",
            }
        if list_only:
            return list_of_legiarti
        all_textes = ""
        nature = ""
        for id in list_of_legiarti:
            article = self.get_article(id)
            if article:
                # print(f"Article {id} found in database")
                all_textes += article["content_html"]
                nature = article["nature"]
            else:
                if debug:
                    print(f"get_all_textes - Article {id} not found in database")
            if len(all_textes) > 20_000:
                # print(f"Warning : {len(all_textes)} characters found in all_textes")
                return {
                    "html": all_textes + "[... to be continued ...]",
                    "nature": nature,
                }
        return {
            "html": all_textes,
            "nature": nature,
        }

    def get_raw_article_json(self, id: str, debug: bool = False):
        """
        Récupère l'article en JSON brut dans la base de données.
        """
        with self.engine.connect() as con:
            sql = self.get_sql_from_id(id, debug=debug)
            if debug:
                print("get_raw_article_json", sql)
            result = con.execute(sql)
            rows = result.all()
            if len(rows) == 0:
                if debug:
                    print(f"get_raw_article_json - Article {id} not found in database")
                return False
            for res in rows:
                return res[1]
            return False

    def get_article(
        self, id: str, raw_article_json: dict = None, debug: bool = False
    ) -> str:
        """
        Retourne le contenu de l'article et des meta-données.
        Ou False si l'article n'est pas trouvé.
        """

        mode_liste = is_list_of_articles(id)

        if raw_article_json is None:
            raw_article_json = self.get_raw_article_json(id, debug=debug)
        if raw_article_json:
            if mode_liste:
                etat = "MULTIPLE"
                res = self.get_all_textes(raw_json=raw_article_json, debug=debug)
                html = res["html"]
                nature = res["nature"]
                date_debut = None
                date_fin = None
                type = None
            else:
                try:
                    etat = raw_article_json["META"]["META_SPEC"]["META_ARTICLE"].get(
                        "ETAT"
                    )
                    html = raw_article_json["BLOC_TEXTUEL"]["CONTENU"]
                    date_debut = raw_article_json["META"]["META_SPEC"]["META_ARTICLE"][
                        "DATE_DEBUT"
                    ]
                    date_fin = raw_article_json["META"]["META_SPEC"]["META_ARTICLE"][
                        "DATE_FIN"
                    ]
                    type = raw_article_json["META"]["META_SPEC"]["META_ARTICLE"].get(
                        "TYPE"
                    )
                    nature = self.get_nature_document(raw_article_json, id)
                except KeyError:
                    # print(f"ERROR : Article {id} do not have key {e}")
                    return False
            id_version_en_vigueur = self.get_id_version_en_vigueur(raw_article_json)
            if id_version_en_vigueur is None:
                en_vigueur = None
            else:
                en_vigueur = id_version_en_vigueur == id
            try:
                note = raw_article_json["NOTA"]["CONTENU"]
            except Exception:
                note = ""
            decret = None
            try:
                for lien in raw_article_json["LIENS"]["LIEN"]:
                    if lien.get("@typelien") == "MODIFIE":
                        decret = {
                            "id": lien.get("@cidtexte"),
                            "date": lien.get("@datesignatexte"),
                            "nom": lien.get("#text"),
                        }
            except KeyError:
                if debug:
                    print(
                        f'get_article {id=} raw_article_json["LIENS"]["LIEN"] not found'
                    )
                pass

            article = {
                "id": id,
                "content_html": html,
                "content_txt": get_text_from_html(html),
                "note": note,
                "titre": self.get_titre(raw_article_json)["long"],
                "titre_court": self.get_titre(raw_article_json)["court"],
                "en_vigueur": en_vigueur,
                "id_version_en_vigueur": id_version_en_vigueur,
                "date_debut": date_debut,
                "date_fin": date_fin,
                "etat": etat,
                "type": type,
                "nature": nature,
                "decret_modificatif": decret,
            }
            return article
        return False

    def get_article_from_url_and_date(self, url: str, date: str, debug: bool = False):
        """
        Retourne l'article en vigueur à la date donnée.
        Car pour l'url https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033817781/2001-01-01
         ce n'est pas l'article LEGIARTI000033817781 qu'il faut aller chercher mais l'article LEGIARTI000006308279
         car c'est lui qui était en vigueur le 2001-01-01.
        """
        id = extract_id_from_url(url)
        if debug:
            print(
                f"get_article_from_url_and_date get_article {id} from {url} and {date=}"
            )
        if id is None:
            return False
        if is_list_of_articles(id):
            """
            Les articles JORFTEXT ont une version
            Les articles LEGISCTA, JORFSCTA n'en ont pas
            """
            if not id.startswith("JORFTEXT"):
                # print(f"get_article_from_url_and_date {id=} ce type d'article n'à pas de version")
                return self.get_article(id, debug=debug)
        article = self.get_raw_article_json(id, debug=debug)
        if article is False:
            if debug:
                print(f"L'article {id} n'a pas été trouvé dans la base.")
            return False
        id_date = find_version_id_from_date(article, date, debug=debug)
        if debug:
            print(f"get_article_from_url_and_date {id_date=}")
        if id_date is None:
            print(
                f"L'article {id} a été trouvé dans la base, mais pas sa correspondance à la date {date} !"
            )
            return False
        # print(f"L'article {id_date} a été trouvé dans la base.")
        return self.get_article(id_date, debug=debug)


########################################################################################################################
if __name__ == "__main__":
    print("All tests passed !")
