import datasets
from llama_recipes.datasets.utils import Concatenator
from transformers import AutoTokenizer

BASE_MODELS = {
    "base7": "meta-llama/Llama-2-7b-hf",
    "chat7": "meta-llama/Llama-2-7b-chat-hf",
    "chat13": "meta-llama/Llama-2-13b-chat-hf",
    "code7": "codellama/CodeLlama-7b-hf",
    "code34": "codellama/CodeLlama-34b-hf",
    "instruct7": "codellama/CodeLlama-7b-Instruct-hf",
    "instruct13": "codellama/CodeLlama-13b-Instruct-hf",
    "instruct34": "codellama/CodeLlama-34b-Instruct-hf",
    # Training 70B requires experimental flag fsdp_peft_cpu_offload_for_save.
    "llama-chat70": "meta-llama/Llama-2-70b-chat-hf",
    "llama-base70": "meta-llama/Llama-2-70b-hf",
    "tiny": "PY007/TinyLlama-1.1B-Chat-v0.1",
    "zephyr": "HuggingFaceH4/zephyr-7b-alpha",
}


# Par exemple, si on vous demande "Dans le texte Quel est le tarif pour les carburéacteurs ?" "Les tarifs normaux, exprimés en euros par mégawattheure, des catégories fiscales des produits taxables en tant que carburant sont les suivants : Gazoles 59,40 Carburéacteurs 59,481."
def format_text(row, tokenizer):
    chat = [
        {
            "role": "system",
            "content": """Vous êtes un juriste travaillant à l'Assemblée Nationale.
    Votre travail consiste à peupler et mettre à jour une base des données nécessaires pour réaliser des simulations fiscales et sociales.
    Pour cela, vous devez prendre les textes réglementaires dans lesquelles figurent les informations à jour que l'on vous fourni, et d'extraire précisément l'information spécifique dont vous avez besoin.
    Vous ne devez extraire que le montant, et uniquement le montant.
    Si vous ne trouver pas la réponse dans le texte vous devez le dire et ne pas chercher à en fournir une autre.
    Avant de répondre, vérifier que la réponse se trouve bien dans le texte indiqué.
            """,
        },
        {
            "role": "user",
            "content": f"Dans le texte suivant, quel est le montant de '{row['description']}' ?\n"
            + f'"""\n{row["texte"]}\n"""\n',
        },
        {
            "role": "assistant",
            "content": "\n[VALUE]\n" + f'{row["value"]}' + "\n[/VALUE]",
        },
    ]
    tokenizer.use_default_system_prompt = False
    prompt = tokenizer.apply_chat_template(chat, tokenize=False)

    return tokenizer(prompt)


def get_openfisca_dataset(data_files: str, tokenizer, split: str):
    full_dataset = datasets.load_dataset(
        "csv",
        data_files=data_files,
    )

    # Since the dataset has no train/test split, we create one and select it
    dataset = full_dataset["train"].train_test_split(
        test_size=0.2,
        shuffle=True,
        seed=42,
    )["train" if split == "train" else "test"]

    dataset = dataset.map(
        lambda x: format_text(x, tokenizer), remove_columns=list(dataset.features)
    )

    dataset = dataset.map(Concatenator(), batched=True, batch_size=None)

    return dataset


def test_dataset(base: str = "tiny"):
    tokenizer = AutoTokenizer.from_pretrained(BASE_MODELS[base])
    tokenizer.add_special_tokens({"pad_token": "<PAD>"})

    BLOCK = "=" * 20

    for split in ["train", "test"]:
        dataset = get_openfisca_dataset(tokenizer, split)
        print(f"{split}: {len(dataset)} sequences")

        sample = tokenizer.decode(dataset[0]["input_ids"])[:500]
        print(f"{BLOCK} Sample {BLOCK}\n{sample} ...")
        print(f"{BLOCK} Tokens {BLOCK}\n{dataset[0]['input_ids'][:25]} ...\n")
