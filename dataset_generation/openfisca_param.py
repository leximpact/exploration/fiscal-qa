import os
import ruamel.yaml
import datetime
from utilitaires import formatNumber, clean_texte
import urllib.parse
import math
import pandas as pd

yaml = ruamel.yaml.YAML()
yaml.width = 800
yaml.preserve_quotes = True
md = ""

MINIMAL_DESCRIPTION_LENGTH = 25


def get_full_description(param_path, root_path):
    """
    Get the full description of an OpenFisca parameter by exploring index.yaml of all the upper branch.
    """
    folders = param_path.split("/")[2:]
    folders
    path_hist = "openfisca_france/parameters/"
    descriptions = []
    for folder in folders:
        path = os.path.join(path_hist, folder)
        if "yaml" in folder:
            index_param = get_param(chemin=path, root_path=root_path)
        else:
            index_param = get_param(chemin=path + "/index.yaml", root_path=root_path)

        descriptions.append(index_param["description"])
        if index_param.get("documentation"):
            descriptions.append(index_param["documentation"])
        path_hist = os.path.join(path_hist, folder)
    return descriptions


def get_values_param_dict(param_dict):
    """
    Get the values of an OpenFisca parameter.
    And keep the last value as a sample_value.
    """
    param_values = {}
    for date, value in param_dict["values"].items():
        date = str(date)
        param_values[date] = value
        sample_value = value
    return {
        "values": param_values,
        "sample_value": sample_value,
    }


def deduplicate_historique(historique):
    """
    De-duplicate if parameter value don't change : For all parameter keep only the first value change, the following values mus be deleted
    """
    keys = list(historique.keys())
    keys.sort()
    keys
    new_historique = {}
    last_value = None
    for date in keys:
        param_at_date = historique[date]
        if last_value == param_at_date["value"]:
            historique[date]["fin"] = param_at_date["fin"]
            continue
        new_historique[date] = param_at_date
        last_value = param_at_date["value"]
    return new_historique


def split_openfisca_parameter(param, output_path):
    """
    Take one OpenFisca parameter and split it into multiple files : one file per parameter.
    """
    for name, content in param.items():
        if name in ("description", "documentation"):
            continue
        print(name)
        if not os.path.exists(f"{output_path}"):
            os.makedirs(f"{output_path}")
        with open(f"{output_path}/{name}.yaml", "w") as f:
            yaml.dump(content, f)


def convert_str_to_date(date_str: str) -> datetime.date:
    date_in_dt = datetime.datetime.strptime(date_str, "%Y-%m-%d")
    return datetime.date(date_in_dt.year, date_in_dt.month, date_in_dt.day)


def create_url_legifrance(
    url_legi: str = None,
    id_legifrance: str = None,
    date: str = None,
    value_in_ref: str = None,
) -> str:
    """
    Create a URL to the Legifrance website.
    """
    if value_in_ref:
        encoded_value = urllib.parse.quote_plus(str(value_in_ref)).replace("+", "%20")
    else:
        encoded_value = None
    if id_legifrance and encoded_value and date:
        url = f"https://www.legifrance.gouv.fr/codes/article_lc/{id_legifrance}/{date}/#:~:text={encoded_value}"
    elif id_legifrance and date:
        url = f"https://www.legifrance.gouv.fr/codes/article_lc/{id_legifrance}/{date}/"
    elif url_legi and date and encoded_value:
        url = f"{url_legi}/{date}/#:~:text={encoded_value}"
    elif url_legi and date:
        url = f"{url_legi}/{date}/"
    elif url_legi and encoded_value:
        url = f"{url_legi}/#:~:text={encoded_value}"
    return url


def check_same_value(value: float, value_in_ref: str) -> bool:
    """
    Check if the value is the same as the value in the reference.
    """
    if not isinstance(value_in_ref, float):
        try:
            value_in_ref = float(value_in_ref.replace("demi", "0.5"))
        except ValueError:
            try:
                value_in_ref = float(value_in_ref.replace(",", ".").replace(" ", ""))
            except ValueError:
                print(f"ValueError: {value_in_ref}")
                return False
    try:
        if value == value_in_ref:
            return True
        elif math.isclose(value * 100, value_in_ref):
            return True
    except ValueError:
        print(f"ValueError: {value} != {value_in_ref}")
        return False
    return False


def df_to_markdown(
    df, mode: str = "", include_list: list = None, exclude_list: list = None
):
    """
    Génère un tableau Markdown à partir d'un DataFrame pour l'inclure dans la description d'une PR
    """
    global md
    md = ""
    df_filtered = df
    if exclude_list:
        df_filtered = df_filtered[~df_filtered["name"].isin(exclude_list)]
    if include_list:
        df_filtered = df_filtered[df_filtered["name"].isin(include_list)]

    def to_md_last_value(row):
        """
        Génére un tableau Markdown pour les cas où il faut juste mettre à jour le champ last_value_still_valid_on.
        """
        global md
        url = row["url_ref_with_value"]
        if pd.isna(url) or url == "" and row["url_ref_0"] != "":
            url = "Valeur non trouvée dans " + create_url_legifrance(
                url_legi=row["url_ref_0"], date=row["last_value_still_valid_on"]
            )
        else:
            url = "Valeur trouvée dans " + create_url_legifrance(
                url_legi=row["url_ref_with_value"],
                date=row["last_value_still_valid_on"],
            )
        md += f"| {row['description']} [{row['name']}]({row['github_url']})| {row['last_value_still_valid_on']} | {row['value']} | {row['value_in_ref']} | {url} |  \r\n"
        return row

    def to_md_new_value(row):
        """
        Génère un tableau Markdown pour les cas où il faut mettre à jour la valeur, en plus de last_value_still_valid_on.
        """
        global md
        if row["value_in_nouveau_ref_found"] == "":
            row["to_change"] = 0
            return row
        url = create_url_legifrance(
            id_legifrance=row["id_version_en_vigueur"],
            date=row["date_application"],
            value_in_ref=row["value_in_nouveau_ref_found"],
        )
        url_field = f'[{row["id_version_en_vigueur"]}]({url})'
        new_value = float(
            str(row["value_in_nouveau_ref_float"]).replace(",", ".").replace(" ", "")
        )
        value_in_nouveau_ref_texte = clean_texte(row["value_in_nouveau_ref_texte"])

        md += f"| {row['description']} [{row['name']}]({row['github_url']}) | {row['value']} | {value_in_nouveau_ref_texte} | {new_value} | {url_field} |\r\n"
        row["to_change"] = 1
        return row

    if mode == "new_value":
        md = "| Paramètre | Ancienne valeur | Extrait |  Nouvelle valeur trouvée | Valeur trouvée dans | \r\n"
        md += "| --- | --- | --- | --- | --- |  \r\n"
        _ = df_filtered.apply(to_md_new_value, axis=1)
    elif mode == "last_value":
        md = "| Paramètre | last_value_still_valid_on | valeur | valeur trouvée dans le texte | Référence |  \r\n"
        md += "| --- | --- | --- | --- | --- |  \r\n"
        _ = df_filtered.apply(to_md_last_value, axis=1)
    else:
        print(f"df_to_markdown - Unknown mode {mode}")
    return md


def sort_CommentedMap(to_sort: ruamel.yaml.CommentedMap) -> ruamel.yaml.CommentedMap:
    date_asc = sorted(list(to_sort))
    sorted_value = ruamel.yaml.CommentedMap()
    for dk in date_asc:
        sorted_value[dk] = to_sort[dk]
    return sorted_value


def get_param(
    chemin: str,
    root_path: str = "../../openfisca-france/",
):
    filepath = os.path.join(root_path, chemin)
    if not os.path.exists(filepath):
        print(f"File not found {filepath}")
        return None
    with open(filepath) as f:
        of_param = yaml.load(f)
    return of_param


def get_values(
    chemin: str,
    root_path: str = "../../openfisca-france/",
):
    filepath = os.path.join(root_path, chemin)
    if not os.path.exists(filepath):
        print(f"File not found {filepath}")
        return None
    with open(filepath) as f:
        of_param = yaml.load(f)
    param_values = {}
    for date, value in of_param["values"].items():
        date = str(date)
        param_values[date] = value
        sample_value = value
    return {
        "values": param_values,
        "sample_value": sample_value,
    }


def update_openfisca_parameter(
    chemin: str,
    last_value_still_valid_on_new: str,
    last_value_still_valid_on_old: str = None,
    id_new_ref: str = None,
    new_value: float = None,
    date_application: str = None,
    titre_court: str = None,
    new_references: dict = None,
    description: str = None,
    short_label: str = None,
    decret: str = None,
    root_path: str = "../../openfisca-france/",
    overwrite: bool = False,
):
    """
    Update an OpenFisca parameter file.
    Args:
        chemin (str): Relative path to the OpenFisca parameter file.
        last_value_still_valid_on_old (str): Old date of the last value still valid.
        last_value_still_valid_on_new (str): New date of the last value still valid.
        id_new_ref (str): ID Legifrance of the new reference.
        new_value (float): New value.
        date_application (str): Date of application of the new value.
        titre_court (str): Short title of the new reference.
        root_path (str): Root path of the OpenFisca project.
    """
    filepath = os.path.join(root_path, chemin)
    if not os.path.exists(filepath):
        print(f"File not found {filepath}")
        return None
    if new_value:
        try:
            new_value = float(str(new_value).replace(",", ".").replace(" ", ""))
            new_value = formatNumber(new_value)
        except Exception:
            print(f"new_value is not a float {new_value}")
            return None
    if date_application == "":
        print(f"date_application is empty for {id_new_ref}")
        return None

    with open(filepath) as f:
        of_param = yaml.load(f)

    if description:
        # S'il y a déjà une description et qu'elle est courte, on la déplace dans short_label
        # et on la remplace par celle donnée en paramètre
        if (
            of_param.get("description")
            and overwrite
            and len(of_param["description"]) < MINIMAL_DESCRIPTION_LENGTH
        ):
            if of_param.get("short_label") is None:
                of_param["metadata"]["short_label"] = of_param["description"]
            of_param["description"] = description
        else:
            of_param["description"] = description

    if short_label:
        if (
            of_param.get("metadata")
            and of_param["metadata"].get("short_label")
            and not overwrite
        ):
            pass
        else:
            if of_param.get("metadata") is None:
                of_param["metadata"] = {}
            of_param["metadata"]["short_label"] = short_label

    if last_value_still_valid_on_old is None:
        if of_param.get("metadata"):
            last_value_still_valid_on_old = of_param["metadata"].get(
                "last_value_still_valid_on"
            )
    if date_application:
        if isinstance(date_application, datetime.date):
            date_application_in_datetime = date_application
        else:
            date_application_in_datetime = convert_str_to_date(date_application)
    # Mise à jour de la référence legifrance
    if new_references and date_application:
        if len(new_references) > 1:
            new_ref = new_references
        else:
            new_ref = new_references[0]
        if of_param["metadata"].get("reference") is None:
            of_param["metadata"]["reference"] = {}

        of_param["metadata"]["reference"][date_application] = new_ref
    if id_new_ref and date_application:
        if of_param["metadata"].get("reference") is None:
            of_param["metadata"]["reference"] = {}
        if of_param["metadata"]["reference"].get(date_application_in_datetime):
            if overwrite is False:
                print("Une référence existe déjà !")
        else:
            new_ref = {
                "title": f"{titre_court}",
                "href": f"https://www.legifrance.gouv.fr/codes/article_lc/{id_new_ref}/{date_application}/",
            }
            if decret:
                new_ref = [
                    new_ref,
                    {
                        "title": decret.get("nom"),
                        "href": f"https://www.legifrance.gouv.fr/jorf/id/{decret.get('id')}",
                    },
                ]
            of_param["metadata"]["reference"][date_application] = new_ref
    # Mise à jour de la valeur
    if new_value and date_application:
        if of_param["values"].get(date_application_in_datetime) and overwrite is False:
            print(
                "Une valeur existe déjà !",
                of_param["values"][date_application_in_datetime]["value"],
            )
        else:
            of_param["values"][date_application] = {"value": "null"}
            of_param["values"][date_application]["value"] = new_value
    with open(filepath, "w") as f:
        yaml.dump(of_param, f)

    with open(filepath) as f:
        content = f.read()
    # Suppression des ' introduite par ruamel.yaml
    content = content.replace(f"'{date_application}'", f"{date_application}")
    # Mise à jour de last_value_still_valid_on
    if content.find("last_value_still_valid_on") > 0:
        content = content.replace(
            f'last_value_still_valid_on: "{last_value_still_valid_on_old}"',
            f'last_value_still_valid_on: "{last_value_still_valid_on_new}"',
        )
    else:
        content = content.replace(
            "metadata:",
            f'metadata:\n  last_value_still_valid_on: "{last_value_still_valid_on_new}"',
        )
    # Ecriture du fichier
    with open(filepath, "w") as f:
        f.write(content)

    # On remet dans l'ordre les dates
    # Ca ne fonctionne pas quand il y a un mix entre les dates et des date en string
    with open(filepath) as f:
        of_param = yaml.load(f)
    if of_param.get("metadata") and of_param["metadata"].get("reference"):
        of_param["metadata"]["reference"] = sort_CommentedMap(
            of_param["metadata"]["reference"]
        )
    of_param["values"] = sort_CommentedMap(of_param["values"])
    with open(filepath, "w") as f:
        yaml.dump(of_param, f)
    return filepath


def recupere_historique(
    id, file_path, of_param_description, legia, legidb, root_path: str = None
):
    # Mise à jour de l'historique
    if root_path:
        param_values = get_values(file_path, root_path)
    else:
        param_values = get_values(file_path)
    sample_value = param_values["sample_value"]
    param_values = param_values["values"]
    raw_article_json = legidb.get_raw_article_json(id)
    legi_versions = legidb.get_versions(raw_article_json)
    values_to_add = {}
    for date, version in legi_versions.items():
        if param_values.get(date) is None:
            values_to_add[date] = version
    article = legidb.get_article(id, raw_article_json)
    for date, value in values_to_add.items():
        article = legidb.get_article(value["id"])
        value["etat"] = article["etat"]
        value["type"] = article["type"]
        value["nature"] = article["nature"]
        llm_answer = legia.llm_find_value(
            description=of_param_description,
            new_legal_text=article["content_html"],
            existing_value=sample_value,
            debug=False,
        )
        value["value"] = llm_answer["value"]
        if value["value"] is None:
            print(f"ERROR for {date}", llm_answer)
            continue
        # print("note=", article.get("note"))
        if len(article.get("note")) < 10:
            value["date_application"] = value["debut"]
            value["titre_court"] = article["titre_court"]
            continue
        metadata = legia.get_date_article(article)
        if metadata is False:
            print(f"Pas de date d'application trouvée pour {date} dans {id}")
            continue
        value["date_application"] = metadata["date"]
        value["titre_court"] = metadata["titre_court"]
        print(
            f"recupere_historique - Adding value {value['value']} for {date} to {file_path}"
        )
    return values_to_add


def save_historique(
    filepath, historique, root_path="../../openfisca-france/", overwrite: bool = False
):
    md = None
    for date, value in historique.items():
        if value["value"] is None or value.get("date_application") is None:
            # print(f"ERROR for {date} : {value=} {value.get('date_application')=}")
            continue
        print(f"Adding version {date} to {filepath}")
        file = update_openfisca_parameter(
            chemin=filepath,
            last_value_still_valid_on_old=None,
            last_value_still_valid_on_new=datetime.date.today().strftime("%Y-%m-%d"),
            id_new_ref=value["id"],
            new_value=value["value"],
            date_application=value["date_application"],
            titre_court=value["titre_court"],
            description=value.get("description"),
            short_label=value.get("short_label"),
            decret=value.get("decret"),
            root_path=root_path,
            overwrite=overwrite,
        )
        if file:
            md = f"    * {file.replace(f'{root_path}openfisca_france', '')}\n"
        else:
            print(f"Erreur lors de la mise à jour de {filepath} pour {date}!")
    return md
