import os
import json
from decouple import Config, RepositoryEnv
import requests
from bs4 import BeautifulSoup
from langchain_core.messages import (
    BaseMessage,
    FunctionMessage,
    HumanMessage,
)
from langchain_core.utils.function_calling import convert_to_openai_function
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langgraph.graph import END, StateGraph
from langgraph.prebuilt.tool_executor import ToolExecutor, ToolInvocation
from typing import Annotated, Sequence, TypedDict, Optional
from langchain_community.tools.tavily_search import TavilySearchResults
import operator
import functools
import tiktoken
from langchain_google_community import GoogleSearchAPIWrapper
from langchain_core.tools import Tool
from langgraph.graph.graph import CompiledGraph
from prompts import (
    reformulator_prompt,
    prompt_format_answer,
    lawer_reference_specialist_prompt,
    lawer_checker_prompt,
    agent_prompt,
    agent_prompt_with_tools,
)
from langchain_openai import ChatOpenAI
from pydantic import BaseModel

# import markdown
# from IPython.display import display, Image
# from ipywidgets import Layout
# import ipywidgets


WITH_QDRANT = False
if WITH_QDRANT:
    from qdrant_client import QdrantClient
    from langchain_community.vectorstores import Qdrant
    from langchain_community.embeddings import HuggingFaceEmbeddings
    from langchain.tools.vectorstore.tool import VectorStoreQATool


# Function to truncate text based on token limit
def truncate_text(text, model_name, max_tokens):
    encoding = tiktoken.encoding_for_model(model_name)
    tokens = encoding.encode(text)
    if len(tokens) > max_tokens:
        tokens = tokens[:max_tokens]
    truncated_text = encoding.decode(tokens)
    return truncated_text


class ParameterExtractionResult(BaseModel):
    reference_url: str
    reference_text: str
    date_application: Optional[str] = None
    valeur: Optional[float] = None
    commentaire: Optional[str] = None


class AgentState(TypedDict):
    """
    # This defines the object that is passed between each node
    # in the graph. We will create different nodes for each agent and tool"""

    messages: Annotated[Sequence[BaseMessage], operator.add]
    sender: str


class OpenFiscaParamAgent:
    def __init__(
        self, env_config: Config, model_name: str = "gpt-4o-mini", debug: bool = False
    ):
        self.model_name = model_name
        self.max_input_tokens = 60_000
        self.debug = debug
        # DOTENV_FILE = "../.env"
        # env_config = Config(RepositoryEnv(DOTENV_FILE))
        self.setup_environment(env_config)

        self.llm = ChatOpenAI(
            model=model_name
        )  # GPT-4o mini supports 128K context and 16K max output tokens

        self.WITH_QDRANT = WITH_QDRANT
        self.search = GoogleSearchAPIWrapper()
        self.all_tools = self.setup_tools(env_config)

        self.tool_executor = ToolExecutor(self.all_tools)
        self.workflow = self.setup_workflow()

    def setup_environment(self, env_config):
        os.environ["TAVILY_API_KEY"] = env_config("TAVILY_API_KEY")
        os.environ["OPENAI_API_KEY"] = env_config("OPENAI_API_KEY")
        os.environ["LANGCHAIN_API_KEY"] = env_config("LANGCHAIN_API_KEY")
        os.environ["GOOGLE_CSE_ID"] = env_config("GOOGLE_SEARCH_ENGINE_ID")
        os.environ["GOOGLE_API_KEY"] = env_config("GOOGLE_SEARCH_API_KEY")
        os.environ["LANGCHAIN_TRACING_V2"] = "true"
        os.environ["LANGCHAIN_PROJECT"] = "OpenFisca Multi-agent Collaboration"

    def reformulate_parameter_description(self, param_description: str = None) -> str:
        answer = self.llm.invoke(reformulator_prompt(param_description))
        # print(answer)
        reformulated = (
            answer.content.replace("<reformulation>", "")
            .replace("</reformulation>", "")
            .strip()
        )
        reformulated = reformulated.replace('"', "")
        reformulated = reformulated.replace("[", "")
        reformulated = reformulated.replace("]", "")
        return reformulated

    def top5_results(self, query):
        return self.search.results(query, 5)

    def extract_content_from_url(self, url):
        """
        Extract the body of an URL.
        This will allow the LLM to browse the web and extract information from web pages.
        """
        try:
            response = requests.get(url)
        except Exception as e:
            print(f"Erreur lors de la récupération de l'URL {url}: {e}")
            return ""
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, "lxml")
            body = soup.find("body")
            the_contents_of_body_without_body_tags = body.findChildren(recursive=False)
            body_html = truncate_text(
                str(the_contents_of_body_without_body_tags[0]),
                self.model_name,
                self.max_input_tokens,
            )
            return body_html
        return ""

    def setup_tools(self, env_config):
        self.google_tool = Tool(
            name="google_search",
            description="Search Google for results about any subjects.",
            func=self.top5_results,
        )
        self.webpage_tool = Tool(
            name="retreive_url_content",
            description="Return the content of an URL.",
            func=self.extract_content_from_url,
        )

        self.tavily_tool = TavilySearchResults(max_results=5)
        all_tools = [self.google_tool, self.webpage_tool, self.tavily_tool]

        if self.WITH_QDRANT:
            # from langchain_core.vectorstores import VectorStoreRetriever
            collection_name = env_config("QDRANT_COLLECTION")
            qdrant_client = QdrantClient(
                env_config("QDRANT_HOST"), port=env_config("QDRANT_PORT"), timeout=3
            )
            # vectors_config = (
            #     {
            #         "titre_vector": models.VectorParams(
            #             size=768, distance=models.Distance.COSINE
            #         ),
            #         "texte_vector": models.VectorParams(
            #             size=768, distance=models.Distance.COSINE
            #         ),
            #     },
            # )
            try:
                count = qdrant_client.count(collection_name)
                print(f"Collection {collection_name} has {count} elements")
            except Exception as e:
                print(e)

            embeddings = HuggingFaceEmbeddings(
                model_name="intfloat/multilingual-e5-base"
            )

            legifrance_vectorstore = Qdrant(
                client=qdrant_client,
                collection_name=collection_name,
                embeddings=embeddings,
                vector_name="texte_vector",
                metadata_payload_key="metadata",
                content_payload_key="data",
            )
            # from langchain.chains.combine_documents import create_stuff_documents_chain
            # from langchain.chains import create_retrieval_chain
            # retriever = VectorStoreRetriever(vectorstore=legifrance_vectorstore)
            # system_prompt = (
            #     "Use the given context to answer the question. "
            #     "If you don't know the answer, say you don't know. "
            #     "Use three sentence maximum and keep the answer concise. "
            #     "Context: {context}"
            # )
            # prompt = ChatPromptTemplate.from_messages(
            #     [
            #         ("system", system_prompt),
            #         ("human", "{input}"),
            #     ]
            # )
            # question_answer_chain = create_stuff_documents_chain(self.llm, prompt)
            # chain = create_retrieval_chain(retriever, question_answer_chain)
            legifrance_tool = VectorStoreQATool(
                name="legifrance_vectorstore",
                description="Prend une phrase en entrée et retourne les textes de loi dans Legifrance qui correspondent le mieux.",
                embeddings=embeddings,
                vectorstore=legifrance_vectorstore,
                vector_name="texte_vector",
                metadata_payload_key="metadata",
                content_payload_key="data",
            )
            all_tools.append(legifrance_tool)

        self.all_tools = all_tools
        self.tool_executor = ToolExecutor(all_tools)
        return all_tools

    def create_agent(self, llm, tools, system_message: str):
        if not tools:
            prompt = ChatPromptTemplate.from_messages(
                [
                    (
                        "system",
                        agent_prompt,
                    ),
                    MessagesPlaceholder(variable_name="messages"),
                ]
            )
            prompt = prompt.partial(system_message=system_message)
            return prompt
        functions = [convert_to_openai_function(t) for t in tools]

        prompt = ChatPromptTemplate.from_messages(
            [
                (
                    "system",
                    agent_prompt_with_tools,
                ),
                MessagesPlaceholder(variable_name="messages"),
            ]
        )
        prompt = prompt.partial(system_message=system_message)
        prompt = prompt.partial(tool_names=", ".join([tool.name for tool in tools]))
        return prompt | llm.bind_functions(functions)

    def agent_node(self, state, agent, name):
        result = agent.invoke(state)
        if isinstance(result, FunctionMessage):
            pass
        else:
            result = HumanMessage(**result.dict(exclude={"type", "name"}), name=name)
        return {
            "messages": [result],
            "sender": name,
        }

    def tool_node(self, state):
        """
        This runs tools in the graph

        It takes in an agent action and calls that tool and returns the result.
        """
        messages = state["messages"]
        # Based on the continue condition
        # we know the last message involves a function call
        last_message = messages[-1]
        # We construct an ToolInvocation from the function_call
        tool_input = json.loads(
            last_message.additional_kwargs["function_call"]["arguments"]
        )
        # We can pass single-arg inputs by value
        if len(tool_input) == 1 and "__arg1" in tool_input:
            tool_input = next(iter(tool_input.values()))
        tool_name = last_message.additional_kwargs["function_call"]["name"]
        action = ToolInvocation(
            tool=tool_name,
            tool_input=tool_input,
        )
        # We call the tool_executor and get back a response
        response = self.tool_executor.invoke(action)
        # We use the response to create a FunctionMessage
        function_message = FunctionMessage(
            content=f"{tool_name} response: {str(response)}", name=action.tool
        )
        # We return a list, because this will get added to the existing list
        return {"messages": [function_message]}

    def router(self, state):
        """
        This is the router, either agent can decide to end
        """
        messages = state["messages"]
        if self.debug:
            print("router state=", state)
        last_message = messages[-1]
        if "function_call" in last_message.additional_kwargs:
            # The previus agent is invoking a tool
            return "call_tool"
        if "FINAL ANSWER" in last_message.content:
            # Any agent decided the work is done
            return "end"
        return "continue"

    def setup_workflow(self) -> CompiledGraph:
        # Research agent and node
        self.lawer_ref_name = "Lawer_reference_specialist"
        research_reference_agent = self.create_agent(
            self.llm,
            self.all_tools,
            system_message=lawer_reference_specialist_prompt,
        )
        research_reference_node = functools.partial(
            self.agent_node, agent=research_reference_agent, name=self.lawer_ref_name
        )

        # lawer_value_name ="Lawer_value_specialist"
        # research_value_agent = create_agent(
        #     llm,
        #     all_tools,
        #     system_message="Vous devez chercher dans le texte la valeur de la loi ou du dispositif spécifié.",
        # )
        # research_value_node = functools.partial(agent_node, agent=research_value_agent, name=lawer_value_name)

        self.lawer_checker_name = "Lawer_checker"
        check_agent = self.create_agent(
            self.llm,
            [self.tavily_tool],
            system_message=lawer_checker_prompt,
        )
        research_checker_node = functools.partial(
            self.agent_node, agent=check_agent, name=self.lawer_checker_name
        )

        workflow = StateGraph(AgentState)
        # Describe the nodes
        # workflow.add_node(reformulator_name, reformulator_node)
        workflow.add_node(self.lawer_ref_name, research_reference_node)
        workflow.add_node(self.lawer_checker_name, research_checker_node)
        workflow.add_node("call_tool", self.tool_node)
        # Set entry point
        # workflow.set_entry_point(reformulator_name)
        workflow.set_entry_point(self.lawer_ref_name)
        # Define the edges
        # workflow.add_conditional_edges(
        #     reformulator_name,
        #     router,
        #     {"continue": lawer_ref_name, "call_tool": "call_tool" },
        # )
        workflow.add_conditional_edges(
            self.lawer_ref_name,
            self.router,
            {"continue": self.lawer_checker_name, "call_tool": "call_tool", "end": END},
        )
        workflow.add_conditional_edges(
            self.lawer_checker_name,
            self.router,
            {"continue": self.lawer_ref_name, "call_tool": "call_tool", "end": END},
        )
        workflow.add_conditional_edges(
            "call_tool",
            # Each agent node updates the 'sender' field
            # the tool calling node does not, meaning
            # this edge will route back to the original agent
            # who invoked the tool
            lambda x: x["sender"],
            {
                self.lawer_ref_name: self.lawer_ref_name,
                self.lawer_checker_name: self.lawer_checker_name,
                # reformulator_name:reformulator_name,
            },
        )

        self.compiled_graph = workflow.compile()

        return self.compiled_graph

    def run_workflow(self, initial_state):
        compiled_graph = self.setup_workflow()
        return compiled_graph.run(initial_state)

    def plot_workflow_graph(graph: CompiledGraph):
        from langchain_core.runnables.graph import MermaidDrawMethod
        from IPython.display import display, Image

        display(
            Image(
                graph.get_graph().draw_mermaid_png(
                    draw_method=MermaidDrawMethod.API,
                )
            )
        )

    def extract_json_from_answer_with_outlines(self, answer: str) -> str:
        """
        NotImplementedError: Cannot use grammar-structured generation with an OpenAI modeldue to the limitations of the OpenAI API.
        """
        import outlines

        model = outlines.models.openai(
            "gpt-4o-mini", api_key=self.env_config("OPENAI_API_KEY")
        )
        generator = outlines.generate.cfg(model, outlines.outlines.grammars.json)
        # generator = outlines.generate.json(model, ParameterExtractionResult)
        result = generator(answer)
        return result

    def extract_json_from_answer(self, answer: str) -> str:
        prompt = prompt_format_answer(answer)
        result = self.llm.invoke(prompt).content
        # Keep only JSON
        result = result.split("{")[1].split("}")[0]
        result = "{" + result + "}"
        # To dict
        try:
            result = json.loads(result)
            result = ParameterExtractionResult(**result)
        except Exception as e:
            print(f"Erreur lors de la conversion en JSON de {result} : {e}")
            return None
        return result

    def call_agent(
        self, question: str, recursion_limit: int = 20
    ) -> ParameterExtractionResult:
        intermediate_result = []
        try:
            i = 0
            for s in self.compiled_graph.stream(
                {
                    "messages": [HumanMessage(content=question)],
                },
                # Maximum number of steps to take in the graph
                {"recursion_limit": recursion_limit},
            ):
                i += 1
                temp = f"<b>Réponse {i}</b>" + str(s)[:200]
                if self.debug:
                    print(temp)
                    print("#" * 80)
                intermediate_result.append(temp)
            # {'Lawer_reference_specialist': {'messages': [HumanMessage(content="FINAL ANSWER\n\nMontant journali
            # Get last answer
            last_answer = s.get(self.lawer_ref_name)
            if last_answer is None:
                last_answer = s.get(self.lawer_checker_name)
            markdown_string = last_answer["messages"][-1].content
            if self.debug:
                print(markdown_string)
            result = self.extract_json_from_answer(markdown_string)
            return result
        except Exception as e:
            error = f"""
                ===========================================================
                {s=}
                ===========================================================
                {e=}
                ===========================================================
                """
            print(error)
            raise e


if __name__ == "__main__":
    # poetry run python dataset_generation/llm_agent.py

    truncated = truncate_text(
        "Bonjour Madame, comment allez-vous ? Impôts sur le revenu , Calcul des",
        model_name="gpt-4o-mini",
        max_tokens=10,
    )
    print(truncated, len(truncated))
    assert len(truncated) == 47

    DOTENV_FILE = ".env"
    env_config = Config(RepositoryEnv(DOTENV_FILE))
    of_agent = OpenFiscaParamAgent(env_config, debug=True)
    of_param_raw_description = """
    Impôts sur le revenu , Calcul des revenus imposables , Déductions sur les salaires, pensions et rentes ,
      Rentes viagères à titre onéreux , À partir de 70 ans.
    """
    question = of_agent.reformulate_parameter_description(of_param_raw_description)
    result = of_agent.call_agent(
        question
        + "Le 2002-01-01 la valeur de ce paramètre était '0.3' il est possible que ce soit toujours la même."
    )
    print(result)
