# Automatisation de la mise à jour des paramètres OpenFisca

L'objectif est de réaliser une automatisation de la mise à jour des paramètres OpenFisca en se basant sur les textes de référence Légifrance.

Fait :
- Prend en compte les textes `LEGISCTA`, `JORFSCTA` et `JORFTEX` car ils sont dans la base tricoteuses mais ne contiennent pas le texte, seulement des références.

Cas exclus pour le moment :
- La construction des barèmes étant complexe, nous ne prenons que la dernière valeur de la dernière tranche.
- S'il y a eu plusieurs modifications, c'est seulement la dernière qui est prise en compte.


Limitations :

- On recherche l'article en vigueur à la date donnée. 
  Car pour l'url https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033817781/2001-01-01 
  ce n'est pas l'article LEGIARTI000033817781 qu'il faut aller chercher mais l'article LEGIARTI000006308279
  car c'est lui qui était en vigueur le 2001-01-01.

  Pour prelevements_sociaux.autres_taxes_participations_assises_salaires.apprentissage.csa.plus_de_250_entre_1_et_2pc lien https://www.legifrance.gouv.fr/codes/id/LEGISCTA000043701827/2022-01-01

  Mais de cette façon, pour le paramètre [prelevements_sociaux.autres_taxes_participations_assises_salaires.apprentissage.apprentissage_taxe_alsace_moselle](https://github.com/openfisca/openfisca-france/blob/f1d846d038575a8154695a4a8bbeaf76f6a707a5/openfisca_france/parameters/prelevements_sociaux/autres_taxes_participations_assises_salaires/apprentissage/apprentissage_taxe_alsace_moselle.yaml) la référence est https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000037368540&cidTexte=JORFTEXT000037367660, qui nous amènes à https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000041472518 
  Alors que l'article qui nous intéresse est https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044981935
  
- Parfois la référence n'est pas en vigueur pour la date de dernier changement de valeur, mais plus récente. On considère dans ce cas qu'elle n'est pas en vigeur alors qu'elle l'est, en quelque sorte...

- Les références législatives ne sont pas toujours correctes : comment vérifier que le title indiqué correspond bien à l'article en lien ? Cf https://github.com/openfisca/openfisca-france/pull/2288#discussion_r1584918953


## Préparation des données

Les fichiers de préparation des données sont les suivants:

- [10_legifrance_articles.ipynb](10_legifrance_articles.ipynb) : Récupération des codes Légifrance à jour depuis le site https://codes.droit.org => Cette partie doit être abandonnée au profit de [tricoteuses](https://git.en-root.org/tricoteuses/data/dila/legi/-/tree/master?ref_type=heads)
- [20_openfisca_parameters.ipynb](20_openfisca_parameters.ipynb) : Récupére les paramètres OpenFisca depuis [leximpact-socio-fiscal-openfisca-json](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-openfisca-json/-/raw/master/raw_processed_parameters.json?ref_type=heads) et les exportent dans des fichiers CSV avec une ligne par paramètres :
  - _"../data/of_param_actifs_avec_ref.csv"_ : paramètres actifs avec une référence pour la dernière valeur déclarée. Dans ce fichier est également indiqué si le champ `last_value_still_valid_on` est à vérifier.
  - _"../data/of_param_neutralises.csv"_ : Paramètres dont la dernière valeur est à 'null'.
  - _"../data/of_param_no_ref.csv"_ : Paramètres sans aucune référence.
  - _"../data/of_param_last_ref_manquante.csv"_ paramètres avec des références, mais pas pour la dernière valeur en date.
- [30_openfisca_vs_legifrance.ipynb](30_openfisca_vs_legifrance.ipynb) : Lit le fichier _"../data/of_param_actifs_avec_ref.csv"_ et l'enrichit du texte Légifrance ou Unédic correspondant dans un fichier appelé [../data/of_param_actifs_avec_ref_legi.csv](../data/of_param_actifs_avec_ref_legi.csv).

## Fichiers résultants

- [../data/of_param_actifs_avec_ref_legi_en_vigeur_avec_valeur_mais_last_value_still_valid_on_perimee.csv](../data/of_param_actifs_avec_ref_legi_en_vigeur_avec_valeur_mais_last_value_still_valid_on_perimee.csv) : 224 paramètres actifs avec une référence pour la dernière valeur déclarée qui est toujours active, mais dont le champ `last_value_still_valid_on` est périmé. => On peut normalement les mettre à jour automatiquement sans risque.
- [../data/of_param_actifs_avec_ref_legi_plus_en_vigeur_avec_valeur.csv](../data/of_param_actifs_avec_ref_legi_plus_en_vigeur_avec_valeur.csv) : 156 paramètres actifs avec une référence pour la dernière valeur déclarée qui n'est plus en vigueur. => La nouvelle référence est indiquée, il faut donc regarder :
  - Si la nouvelle référence contient la même valeur il faut juste mettre à jour le champ `last_value_still_valid_on`.
  - Si la nouvelle référence ne contient pas la valeur, il faut chercher la nouvelle valeur. Et mettre à jour le champ `last_value_still_valid_on`.

## Cas des paramètres avec référence en vigeur

Pour les paramètres actifs avec une référence pour la dernière valeur déclarée qui est toujours active, mais dont le champ `last_value_still_valid_on` est périmé, il est possible de les mettre à jour automatiquement sans risque.

Dans un premier temps, se limiter aux textes où on retrouve la valeur.

Comme c'est en phase de test, il est préférable conserver une revue humaine. Pour qu'elle soit digeste, il est possible de :
- Regrouper les paramètres par partie d'arbre, car il est organisé d'un point de vue métier. Avec une vingtaine maximum.
- Ajouter un tablau Markdown avec le nom du paramètre, le lien vers l'article le texte de référence Légifrance.
- Expliquer la démarche dans la PR.

Apres la phase de test, on peut envisager une revue du script de mise à jour automatique.

## Interface graphique pour mettre à jour les paramètres OpenFisca

Le notebook [openfisca_interface.ipynb](openfisca_interface.ipynb) permet de visualiser les paramètres OpenFisca necessitant une mise à jour en affichant le texte de référence Légifrance, la possibilité de se faire aider par un LLM ainsi q'un lien vers le gihub OpenFisca et l'interface de mise à jour des paramètres :

![alt text](img/image.png)

## Interface graphique pour mettre à jour les dates de validité des paramètres OpenFisca

[openfisca_last_value.ipynb](openfisca_last_value.ipynb) présentes les paramètres OpenFisca dont la valeur est à vérifier car le champ `last_value_still_valid_on` date de plus d'un an.

L'idée est d'automatiser la vérification :

- Ouvrir la page de référence.
- Vérifier si elle contient la valeur du paramètre, car sinon c'est inutile d'aller plus loin.
- Voir si elle est toujours en vigueur.
- Sinon récupérer la page de l'article en vigueur.
- Récupérer la nouvelle valeur.
- Ouvrir une PR de mise à jour de `last_value_still_valid_on`.

![alt text](img/last_value.png)

Ici nous voyons que la valeur a changée dans le nouvelle article et que le LLM a bien trouvé la nouvelle valeur.

## Cas où il n'y a pas de référence

Un essais a été fait avec [cohere](https://coral.cohere.com/) et son onglet `grounding` qui permet de forcer la recherche dans légifrance.

Voici le prompt utilisé :

```txt
Vous êtes un juriste travaillant à l'Assemblée Nationale. Votre travail consiste à peupler et mettre à jour une base des données nécessaires pour réaliser des simulations fiscales et sociales. Pour cela, vous devez prendre les textes réglementaires dans lesquelles figurent les informations à jour que l'on vous fourni, et d'extraire précisément l'information spécifique dont vous avez besoin. Vous devez extraire la date de dernière mise à jour, et véfifier que sa valeur est toujours la même. Notez bien que les pourcentage sont exprimés sous forme mathématique, donc une valeur de 25% dans le texte de loi sera noté 0.25 dans notre système. Si vous ne trouvez pas la réponse dans le texte vous devez chercher une correspondance approchante. S'il n'y en a vraiment pas, il faut le dire et ne pas chercher à en fournir une autre. Avant de répondre, vérifier que la réponse se trouve bien dans le texte indiqué. Est-ce que la valeur de 'Réduction pour enfants scolarisés au lycée' est toujours '153' ?
```

![alt text](img/cohere.png)

Effectivement [l'article proposé en référence](https://www.legifrance.gouv.fr/codes/id/LEGISCTA000006191829) contenait bien la bonne valeur et était à jour.

## Utilisation de tricotteuse

Les données de tricoteuse Légifrance sont disponibles dans [le GitLab tricoteuses](https://git.en-root.org/tricoteuses/data/dila), qui est chargé dans une base Postgres.

Par exemple pour l'article https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042907517/2021-01-01 qui est référencé par [impot_revenu.calcul_impot_revenu.plaf_qf.decote.taux](https://github.com/openfisca/openfisca-france/blob/master/openfisca_france/parameters/impot_revenu/calcul_impot_revenu/plaf_qf/decote/taux.yaml) mais qui est remplacé par https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000048805432/2024-01-01/

Cela se voit facilement dans le XML `global/code_et_TNC_en_vigueur/code_en_vigueur/LEGI/TEXT/00/00/06/06/95/LEGITEXT000006069577/article/LEGI/ARTI/00/00/42/90/75/LEGIARTI000042907517.xml` :

```xml
<VERSION etat="VIGUEUR">
<LIEN_ART debut="2024-01-01" etat="VIGUEUR" fin="2999-01-01" id="LEGIARTI000048805432" num="197" origine="LEGI"/>
</VERSION>
```

On trouve donc ici une balise avec la propriété `etat="VIGUEUR"` et un lien vers l'article `LEGIARTI000048805432` qui est en vigueur à partir du `2024-01-01`.

De même en ouvrant `global/code_et_TNC_en_vigueur/code_en_vigueur/LEGI/TEXT/00/00/06/06/95/LEGITEXT000006069577/article/LEGI/ARTI/00/00/48/80/54/LEGIARTI000048805432.xml`, il référence l'ancien article:

```xml
<VERSION etat="MODIFIE">
<LIEN_ART debut="2020-12-31" etat="MODIFIE" fin="2022-01-01" id="LEGIARTI000042907517" num="197" origine="LEGI"/>
</VERSION>
```

Une recherche dans l'arborescence prend plusieurs secondes, sans même aller voir dans le contenu des fichiers, il est donc préférable d'utiliser la base Postgres pour accélérer les recherches.

#### Procédure de mise à jour

```ssh
scp legi.sql.gz serveur:/data/private-data/output/tricoteuses/
ssh serveur
pct enter 203
docker exec -it postgres /bin/bash
psql -U postgres -c "DROP DATABASE IF EXISTS legi;" && createdb -U postgres legi
createuser legi
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS textelr;
psql -U postgres legi -f /mnt/data-out/tricoteuses/legi.sql
# gunzip -c /mnt/data-out/tricoteuses/legi.sql.gz | psql -U postgres legi
psql -U postgres legi
\d
```

### Cas des décrets

Parfois la référence est un décret qui ne contient pas nécessairement la valeur du paramètre. Dans ce cas, une autre référence donne parfois le bon article. Sinon il faut trouver l'article en vigueur à partir du décret.

## Risque d'avalanche de PR

⚠️ potentiellement cette automatisation va générer beaucoup de PR, il faudrait donc un moyen de les regrouper. Par exemple en les validant dans un format tableau avant d'ouvrir une PR commune.

## Statistiques

Dans OpenFisca, en avril 2024 il y a 2 735 paramètres, touvés par la commande `find openfisca_france/parameters | grep -v index | grep yaml | wc -l`

Notre outil en trouve :
- 2660 en tout.
- 1955 avec au moins une référence.
- 1605 avec une valeur et des références, mais pas forcément pour la dernière valeur.
- 1587 actifs avec une référence pour la dernière valeur déclarée.
- 966 neutralisés : valeur à null.
- 807 non neutralisés et sans aucune référence.
- 102 avec des références, mais pas pour la dernière valeur.

Parmi les 1587 paramètres actifs avec une référence pour la dernière valeur déclarée, il y en a :
 - 365 avec un last_value_still_valid_on de moins d'un an.
 - 512 avec un last_value_still_valid_on de plus d'un an.
 - 710 sans last_value_still_valid_on.

Il y a 1161 paramètres actifs avec une référence qui est trouvée dans la base Légi de tricoteuse.
- Dont 203 paramètres dont la référence est toujours en vigueur.
  * Dont 92 CODES
  * Dont 86 LOIS
  * Dont 23 DECRETS
  * Dont 2 ARRETES
- Dont 397 paramètres dont la référence n'est plus en vigueur.
  * Dont 238 CODES
  * Dont 80 ARRETES
  * Dont 72 LOIS
  * Dont 7 DECRETS
- Dont 561 paramètres dont l'état de la référence est incertain (arrêté par exemple).

 Il y a 205 paramètres avec une référence de nature 'Code' qui n'est plus en vigueur et dont la valeur dans le nouvel article en vigueur est retrouvée. On peut donc proposer de mettre à jour le champ `last_value_still_valid_on`.


Il y a 46 paramètres actifs avec une référence de nature _Code_ qui est toujours en vigueur mais un _last_value_still_valid_on_ qui a plus d'un an.

Il y a 43 paramètres dont la référence n'est plus en vigueur, dont on trouvait la valeur dans la référence et qu'elle n'est pas retrouvée dans le nouveau. Il faut donc aller la chercher avec une IA. Finalement, après contrôle automatique, il y a eu 29 paramètres modifiés par IA.
