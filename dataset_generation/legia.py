from unittest import TestCase
from llm import get_completion
from utilitaires import extract_dates, extract_floats
import prompts
import json

tc = TestCase()


class LegIA:
    def __init__(self, model="mistral-large-latest"):
        self.model = model

    system_prompt_value_extraction = prompts.system_prompt_value_extraction
    system_prompt_date_extraction = prompts.system_prompt_date_extraction

    def llm_find_date_application(self, text: str):
        if len(text) < 10:
            return {
                "question": text,
                "answer": None,
                "date": None,
                "error": "Texte trop court",
            }

        user_prompt = f'Quelle est la date d\'application de la loi suivante ?\n\n"""\n{text}\n"""\n'

        answer = get_completion(
            user_prompt,
            model=self.model,
            system_prompt=self.system_prompt_date_extraction,
        )
        try:
            return {
                "question": user_prompt,
                "answer": answer,
                "date": extract_dates(answer)[-1],
                "error": None,
            }
        except Exception as e:
            return {
                "question": user_prompt,
                "answer": answer,
                "date": None,
                "error": str(e),
            }

    def llm_for_value(self, system_prompt: str, user_prompt: str, debug: bool = False):
        # GPT
        # answer = get_completion(user_prompt)
        answer = get_completion(
            user_prompt, model=self.model, system_prompt=system_prompt
        )
        final_answer = None
        try:
            # json load
            if isinstance(answer, dict):
                final_answer = answer["valeur"]
            else:
                answer = answer.replace("```json", "").replace("```", "")
                answer = json.loads(answer)
                final_answer = answer["valeur"]
        except Exception as e:
            if debug:
                print("llm_for_value - JSON error : ", answer, e)
            if answer is None:
                final_answer = None
            else:
                start = answer.find("a nouvelle valeur est :")
                if start != -1:
                    start = len("a nouvelle valeur est :")
                    final_answer = answer[start:]
        if final_answer is None:
            if debug:
                print("Pas de valeur trouvée !", answer)
            return {
                "question": system_prompt + "\n------------------\n" + user_prompt,
                "answer": answer,
                "value": None,
                "error": "Pas de valeur trouvée !",
            }
        try:
            return {
                "question": system_prompt + user_prompt,
                "answer": answer,
                "value": extract_floats(final_answer, debug)[-1],
                "error": None,
            }
        except Exception as e:
            if debug:
                print(answer, e)
            return {
                "question": system_prompt + "\n------------------\n" + user_prompt,
                "answer": answer,
                "value": None,
                "error": str(e),
            }

    def llm_find_new_value_from_old_value(
        self,
        description: str,
        previous_legal_text: str,
        new_legal_text: str,
        previous_value: str,
        debug: bool = False,
    ):
        user_prompt = (
            f"Dans le texte suivant, quelle est la valeur de '{description}' sachant que la valeur précédente était '{previous_value}' ?\n"
            + f'\n"""\n{new_legal_text}\n"""\n'
            + "Le contenu du texte précédent, qui n'est plus en vigeur aujourd'hui, était :\n"
            + f'\n"""\n{previous_legal_text}\n"""\n'
        )
        return self.llm_for_value(
            self.system_prompt_value_extraction, user_prompt, debug
        )

    def llm_find_value(
        self,
        description: str,
        new_legal_text: str,
        existing_value: str,
        debug: bool = False,
    ):
        user_prompt = (
            f"Dans le texte suivant, quelle est la valeur de '{description}' sachant que la valeur à une autre date de validité était '{existing_value}' ?\n"
            + f'\n"""\n{new_legal_text}\n"""\n'
        )
        return self.llm_for_value(
            self.system_prompt_value_extraction, user_prompt, debug
        )

    def get_date_article(self, article: str):
        note = article.get("note")
        reponse = self.llm_find_date_application(note)
        date_debut = reponse.get("date")
        if date_debut is None:
            print("WARNING", reponse)
            if article.get("date_debut"):
                return {
                    "date": article.get("date_debut"),
                    "id": article.get("id"),
                    "titre_court": article.get("titre_court"),
                    "note": note,
                }
            if article.get("decret_modificatif") is not None:
                return {
                    "date": article.get("decret_modificatif").get("date"),
                    "id": article.get("decret_modificatif").get("id"),
                    "titre_court": article.get("titre_court"),
                    "note": note,
                }
            return False
        return {
            "date": date_debut,
            "id": article.get("id"),
            "titre_court": article.get("titre_court"),
            "note": note,
        }


if __name__ == "__main__":
    legia = LegIA()
    # Test 1
    result = legia.llm_find_date_application(
        "La loi du 12/03/2022 est applicable à partir du 01/01/2023."
    )
    tc.assertEqual(result["date"], "2023-01-01")
    # Test 2
    result = legia.llm_find_new_value(
        description="le taux de TVA",
        previous_legal_text="Le taux de TVA est de 0.2.",
        new_legal_text="Le taux de TVA est de 0.25.",
        previous_value="0.2",
        debug=True,
    )
    tc.assertEqual(result["value"], 0.25)
    print("Tests pass.")
