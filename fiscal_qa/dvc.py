# AUTOGENERATED! DO NOT EDIT! File to edit: ../notebooks/dvc.ipynb.

# %% auto 0
__all__ = []

# %% ../notebooks/dvc.ipynb 2
import argparse

# %% ../notebooks/dvc.ipynb 3
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="interface between fiscal_qa library and DVC"
    )
    subparsers = parser.add_subparsers(dest="subparser")

    extract_parser = subparsers.add_parser("extract")
    download_parser = subparsers.add_parser("download")
    finetune_parser = subparsers.add_parser("finetune")
    subparsers.add_parser("evaluate")

    kwargs = vars(parser.parse_args())
    if kwargs.pop("subparser") is None:
        parser.print_help()
    else:
        globals()[kwargs.pop("subparser")](**kwargs)
