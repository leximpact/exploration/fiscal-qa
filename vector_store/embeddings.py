import numpy as np
import torch
import torch.nn.functional as F
from torch import Tensor
from transformers import AutoModel, AutoTokenizer


class Embeddings:
    def __init__(self, disable_gpu: bool = False) -> None:
        self.with_gpu = False
        self.device_map = None
        if not disable_gpu and torch.cuda.is_available():
            self.with_gpu = True
            self.device_map = "auto"  # "cuda:0"

        self.model_name = "intfloat/multilingual-e5-base"

        # Load model
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_name)
        self.model = AutoModel.from_pretrained(
            self.model_name, device_map=self.device_map
        )  # -> SentenceTransformer

    def average_pool(
        self, last_hidden_states: Tensor, attention_mask: Tensor
    ) -> Tensor:
        last_hidden = last_hidden_states.masked_fill(
            ~attention_mask[..., None].bool(), 0.0
        )
        return last_hidden.sum(dim=1) / attention_mask.sum(dim=1)[..., None]

    def embed(self, texts, batch_size=1):  # 4 on colab GPU...
        # Sentence transformers for E5 like model
        embeddings = []
        for i in range(0, len(texts), batch_size):
            batch_dict = self.tokenizer(
                texts[i : i + batch_size],
                max_length=512,
                padding=True,
                truncation=True,
                return_tensors="pt",
            )
            if self.with_gpu:
                batch_dict.to("cuda")

            outputs = self.model(**batch_dict)

            vectors = self.average_pool(
                outputs.last_hidden_state, batch_dict["attention_mask"]
            )
            vectors = F.normalize(vectors, p=2, dim=1)
            # print(type(vectors)) -> Tensor
            # print(vectors.shape) -> (n_doc X size_embedding)
            if self.with_gpu:
                embeddings.append(vectors.detach().cpu().numpy())
            else:
                embeddings.append(vectors.detach().numpy())
            # torch.cuda.empty_cache()

        # return torch.cat(embeddings) # burn memory
        return np.vstack(embeddings)
