from decouple import Config, RepositoryEnv
import openai
from transformers import AutoTokenizer
import transformers
import torch
import json
import re
import os
from math import isclose
import requests
import time
from mistralai.client import MistralClient
from mistralai.models.chat_completion import ChatMessage
from tenacity import (
    retry,
    stop_after_attempt,
    wait_random_exponential,
)  # for exponential backoff

# Pour Albert, le modèle de la DINUM
albert_url = ""
albert_token = None
albert_tokenizer = None

DOTENV_FILE = "../.env"
env_config = Config(RepositoryEnv(DOTENV_FILE))
openai.api_key = env_config("OPENAI_API_KEY")
mistral_client = MistralClient(api_key=env_config("MISTRAL_API_KEY"))

system_prompt = """Vous êtes un juriste travaillant à l'Assemblée Nationale.
        Votre travail consiste à peupler et mettre à jour une base des données nécessaires pour réaliser des simulations fiscales et sociales.
        Pour cela, vous devez prendre les textes réglementaires dans lesquelles figurent les informations à jour que l'on vous fourni, et d'extraire précisément l'information spécifique dont vous avez besoin.
        Vous ne devez extraire que le montant, et uniquement le montant.
        Si vous ne trouver pas la réponse dans le texte vous devez le dire et ne pas chercher à en fournir une autre.
        Avant de répondre, vérifier que la réponse se trouve bien dans le texte indiqué.
                """


@retry(wait=wait_random_exponential(min=1, max=30), stop=stop_after_attempt(6))
def get_completion(prompt, model="albert", system_prompt=system_prompt):
    """
    Get the completion of a prompt from the OpenAI API
    Args:
        prompt (str): the prompt to complete
        model (str): the model to use for completion (default: gpt-3.5-turbo) gpt-4
    """

    if model == "albert":
        return ask_albert(system_prompt, prompt)
    if "mistral" in model:
        # See https://docs.mistral.ai/platform/endpoints/#mistral-ai-generative-models
        chat_response = mistral_client.chat(
            model=model,
            messages=[ChatMessage(role="user", content=system_prompt + "\n" + prompt)],
        )
        return chat_response.choices[0].message.content

    messages = [
        {"role": "system", "content": system_prompt},
        {"role": "user", "content": prompt},
    ]
    try:
        response = openai.ChatCompletion.create(
            model=model,
            messages=messages,
            temperature=0,  # this is the degree of randomness of the model's output
        )
        return response.choices[0].message["content"]
    except openai.error.RateLimitError as e:
        retry_after = int(e.headers.get("retry-after", 60))
        print(f"Rate limit exceeded, waiting for {retry_after} seconds...")
        time.sleep(retry_after)
        return get_completion(prompt, model=model)


def albert_login():
    global albert_url, albert_token, albert_tokenizer
    albert_tokenizer = AutoTokenizer.from_pretrained("PY007/TinyLlama-1.1B-Chat-v0.3")
    credentials_file = os.path.join(os.path.dirname(__file__), "credentials.json")
    with open(credentials_file, "r") as f:
        cred = json.load(f)
        username, password, albert_url = cred["username"], cred["password"], cred["url"]
    # Sign In:
    url = f"{albert_url}/sign_in"
    response = requests.post(
        url, json={"username": username, "password": password}, timeout=1
    )
    if response.status_code != 200:
        raise Exception(
            f"Unable to connect to Albert : {response.status_code} {response.text}"
        )
    # print(response.text)
    albert_token = response.json()["token"]
    return albert_token


def albert_user_create(name, email, password):
    global albert_token
    if not albert_token:
        albert_token = albert_login()
    try:
        headers = {
            "Authorization": f"Bearer {albert_token}",
        }
        data = {
            "username": name,
            "email": email,
            "password": password,
            "is_confirmed": True,
        }
        url = f"{albert_url}/user/confirm"
        response = requests.post(url, json=data, headers=headers, timeout=3)
        if response.status_code != 200:
            raise Exception(f"ERROR {response.status_code=} {response.text}")
        return response.json()
    except Exception as e:
        print(f"ERROR in albert_user_confirm : {e} {url=}")
        time.sleep(1)
        raise e


def albert_user_pending():
    global albert_token
    if not albert_token:
        albert_token = albert_login()
    try:
        headers = {
            "Authorization": f"Bearer {albert_token}",
        }

        url = f"{albert_url}/users/pending"
        response = requests.get(url, headers=headers, timeout=3)
        if response.status_code != 200:
            raise Exception(f"ERROR {response.status_code=} {response.text}")
        return response.json()
    except Exception as e:
        print(f"ERROR in albert_user_pending : {e} {url=}")
        time.sleep(1)
        raise e


def albert_user_confirm(email):
    global albert_token
    if not albert_token:
        albert_token = albert_login()
    try:
        headers = {
            "Authorization": f"Bearer {albert_token}",
        }
        data = {"email": email, "is_confirmed": True}
        url = f"{albert_url}/user/confirm"
        response = requests.post(url, json=data, headers=headers, timeout=3)
        if response.status_code != 200:
            raise Exception(f"ERROR {response.status_code=} {response.text}")
        return response.json()
    except Exception as e:
        print(f"ERROR in albert_user_confirm : {e} {url=}")
        time.sleep(1)
        raise e


def ask_albert(system_prompt, prompt):
    global albert_token
    if not albert_token:
        albert_token = albert_login()
    try:
        headers = {
            "Authorization": f"Bearer {albert_token}",
        }
        query = system_prompt + "\n---\n" + prompt
        tokens = albert_tokenizer.encode(query)
        len(tokens)
        # Define the maximum number of tokens allowed.
        max_tokens = 2_500
        # Truncate the tokens if necessary.
        truncated_tokens = tokens[:max_tokens]
        # Convert the truncated tokens back to text.
        query = albert_tokenizer.decode(
            truncated_tokens, clean_up_tokenization_spaces=True
        )
        # print(f"headers:{headers}")
        # Create Stream:
        data = {
            "context": "",
            "institution": "",
            "limit": 1,
            "links": "",
            "mode": "simple",
            "model_name": "albert-light",
            "query": query,
            "temperature": 1,
            "user_text": "",
        }
        # print(data)
        url = f"{albert_url}/stream"
        response = requests.post(url, json=data, headers=headers, timeout=3)
        if response.status_code != 200:
            raise Exception(f"ERROR {response.status_code=} {response.text}")
        if not response.json().get("id"):
            print("ERROR", response.json())
            return ""
        stream_id = response.json()["id"]
        # print(f"stream_id:{stream_id}")

        # Start Stream:
        data = {"stream_id": stream_id}
        url = f"{albert_url}/stream/{stream_id}/start"
        response = requests.get(
            url, json=data, headers=headers, stream=True, timeout=20
        )
        if response.status_code != 200:
            raise Exception(f"ERROR {response.status_code=} {response.text}")
        # print("-> Waiting for the response stream:")
        response_texte = ""
        for line in response.iter_lines():
            if not line:
                continue
            _, _, data = line.decode("utf-8").partition("data: ")
            text = json.loads(data)
            if text == "[DONE]":
                break
            response_texte += text
            # print(text, end="", flush=True)
        time.sleep(0.5)
        return response_texte
    except Exception as e:
        print(f"ERROR in ask_albert : {e} {url=}")
        time.sleep(1)
        raise e


def extract_all_floats(string):
    """Extracts all floats from a string, including those with commas.

    Args:
      string: The string to extract floats from.

    Returns:
      A list of all floats extracted from the string.
    """
    # Replace spaces and commas in numbers
    text = re.sub(r"(\d) (\d)", r"\1\2", string)  # For space-separated numbers
    text = re.sub(r"(\d),(\d)", r"\1.\2", text)  # For comma as a decimal separator

    # Use regex to extract numbers
    pattern = r"-?\d+(?:\.\d+)?"
    numbers = re.findall(pattern, text)

    cleaned_numbers = [float(num) for num in numbers]

    return cleaned_numbers


def is_float_in_list(number, float_list) -> bool:
    for f in float_list:
        if isclose(f, number):
            return True
    else:
        return False


def is_number_in_text(number, texte):
    float_list = extract_all_floats(texte)
    return is_float_in_list(number, float_list)


assert is_number_in_text(10, "Il y a 10% !") is True
assert is_number_in_text(10, "Il y a 10.0 ans !") is True
assert is_number_in_text(10.5, "Il y a 10,50 ans !") is True


class Chat:
    def __init__(self, model_name_or_path=None):
        # See https://github.com/huggingface/blog/blob/main/llama3.md
        self.model_name_or_path = model_name_or_path
        self.tokenizer = AutoTokenizer.from_pretrained(model_name_or_path)

        self.pipeline = transformers.pipeline(
            "text-generation",
            model=model_name_or_path,
            model_kwargs={"torch_dtype": torch.bfloat16},
            device_map="auto",
        )
        # You can also automatically quantize the model, loading it in 8-bit or even 4-bit mode. 4-bit loading takes about 7 GB of memory to run, making it compatible with a lot of consumer cards and all the GPUs in Google Colab. This is how you’d load the generation pipeline in 4-bit:

        # pipeline = transformers.pipeline(
        #     "text-generation",
        #     model=model_id,
        #     model_kwargs={
        #         "torch_dtype": torch.float16,
        #         "quantization_config": {"load_in_4bit": True},
        #         "low_cpu_mem_usage": True,
        #     },
        # )

    def format_prompt(self, article: str, description: str, system_prompt: str = None):
        if not system_prompt:
            system_prompt = """Vous êtes un juriste travaillant à l'Assemblée Nationale.
        Votre travail consiste à peupler et mettre à jour une base des données nécessaires pour réaliser des simulations fiscales et sociales.
        Pour cela, vous devez prendre les textes réglementaires dans lesquelles figurent les informations à jour que l'on vous fourni, et d'extraire précisément l'information spécifique dont vous avez besoin.
        Vous ne devez extraire que le montant, et uniquement le montant.
        Si vous ne trouver pas la réponse dans le texte vous devez le dire et ne pas chercher à en fournir une autre.
        Avant de répondre, vérifier que la réponse se trouve bien dans le texte indiqué.
                """
        if description:
            chat = [
                {"role": "system", "content": system_prompt},
                {
                    "role": "user",
                    "content": f"Dans le texte suivant, quel est le montant de '{description}' ?\n"
                    + f'"""\n{article}\n"""\n',
                },
            ]
        else:
            chat = [
                {"role": "system", "content": system_prompt},
                {"role": "user", "content": f"{article}"},
            ]
        if "ixtral" in self.model_name_or_path:
            # Mixtral model don't support system_prompt
            chat = [
                {
                    "role": "user",
                    "content": f"{chat[0]['content']}\n{chat[1]['content']}",
                },
            ]
        # self.tokenizer.use_default_system_prompt = False
        # prompt = self.tokenizer.apply_chat_template(chat, tokenize=False)

        return chat

    def ask_llm(
        self,
        texte,
        description,
        system_prompt: str = None,
        top_k=5,
        top_p: float = 0.7,
        temperature=0.2,
        max_new_tokens=50,
        retry_allowed: int = 1,
        debug: bool = False,
    ) -> str:
        """
        Ask the answer to the model.
        """
        messages = self.format_prompt(texte, description, system_prompt)
        terminators = [
            self.pipeline.tokenizer.eos_token_id,
            self.pipeline.tokenizer.convert_tokens_to_ids("<|eot_id|>"),
        ]

        outputs = self.pipeline(
            messages,
            eos_token_id=terminators,
            do_sample=True,
            top_k=top_k,
            top_p=top_p,
            num_return_sequences=1,
            repetition_penalty=1.1,
            temperature=temperature,
            max_new_tokens=max_new_tokens,
            # pad_token_id=generator.tokenizer.eos_token_id
        )
        if debug:
            print("Chat.ask_llm.outputs", outputs)
        try:
            assistant_response = outputs[0]["generated_text"][-1]["content"]
        except Exception as e:
            print(f"ERREUR {e} {outputs=}")
            raise e
        if debug:
            print("Chat.ask_llm.assistant_response", assistant_response)

        # sequences = self.pipeline(
        #     self.format_prompt(texte, description, system_prompt),
        #     do_sample=True,
        #     top_k=top_k,
        #     top_p=top_p,
        #     num_return_sequences=1,
        #     repetition_penalty=1.1,
        #     temperature=temperature,
        #     max_new_tokens=max_new_tokens,
        # )
        # answer = sequences[0]["generated_text"]
        return assistant_response

    def llm_extract_float(
        self,
        texte,
        description,
        top_k=5,
        top_p: float = 0.7,
        temperature=0.2,
        max_new_tokens=50,
        retry_allowed: int = 1,
        debug=False,
    ) -> float:
        """
        Ask the answer to the model.
        If the answer is not in the text, try again 6 times.
        """
        number = None
        # Allow n retries
        for i in range(retry_allowed):
            answer = self.ask_llm(
                texte,
                description,
                system_prompt=None,
                top_k=top_k,
                top_p=top_p,
                temperature=temperature,
                max_new_tokens=max_new_tokens,
                retry_allowed=retry_allowed,
                debug=debug,
            )
            pattern = r"\[VALUE\]\s*([-+]?\d*\.\d+|\d+)\s*\[\/VALUE\]"
            match = re.search(pattern, answer)
            if match:
                number = float(match.group(1))
                if is_number_in_text(number, texte):
                    return {
                        "number": number,
                        "answer": answer,
                        "after_n_try": i + 1,
                    }
        else:
            # Cas des modèles non fine-tunés
            if "[/INST]" in answer:
                answer = answer[answer.find("[/INST]") :].replace("[/INST]", "")
            number = extract_all_floats(answer)
            if debug:
                print(
                    "Chat.llm_extract_float",
                    answer,
                    " -> extract_all_floats=",
                    number,
                )
            if len(number) > 0:
                number = number[0]
            else:
                number = None
            return {
                "answer": answer,
                "number": number,
                "after_n_try": i + 1,
            }
